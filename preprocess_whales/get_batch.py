import numpy as np
import scipy 

import skimage
from skimage import transform as trans

import matplotlib.pyplot as plt
import matplotlib
import pdb

import csv
from my_utils import *

matplotlib.use('Agg')

def get_batch(num_images = 100, transforms = None, img_size = [100, 100], start_index = 0, random_shuffle = False):
    """ 
        Returns a set of stacks of images, and a list of lists with labels.
        
        If transforms is None:
            -- only one stack is returned of size img_size[0] x img_size[1] x num_images x 1
        
        If transforms is <tuple of transforms>:
            -- the following transforms are carried out:
                 * ROTATEX  : rotate at angles of 90, 180, 270
                 * HUE      : add a value between -0.1 and 0.1 to the hue component (Dosovitskiy et al.)
                 * CONTRAST : raise S and V to a power from 0.25 and 4, multiply
                             these by a factor between 0.7 and 1.4, add them to
                             a value between -0.1 and 0.1. (Dosovitskiy et al.)
            -- a set of |transforms| stacks are returned, with each stack corresponding to a 
               separate transform (the first set is an untransformed stack).
        
        Images are resized to img_size[0] x img_size[1].

        Images are read from start_index to (start_index + num_images - 1).

        If random_shuffle is True, a random set of 100 images are chosen.

    """

    training_set_dir = '/data/lisatmp4/whales/data/imgs/'
    f = csv.reader(open('/data/lisatmp4/whales/data/train.csv'))
    
    training_files = []
    first_time = 1
    for row in f:
        if first_time:
            first_time = 0
            continue
        training_files.append((row[0], row[1]))

    total_images = len(training_files)
    if random_shuffle == False:
        indices = range(start_index, num_images)
    else:
        indices = np.arange(total_images)
        np.random.shuffle(indices)
        indices = indices[:num_images]

    if not transforms:
        stack_of_images = np.zeros((img_size[0], img_size[1], 3, num_images, 1))
        labels = [''] * num_images
        for i, ind in zip(range(num_images), indices):
            stack_of_images[:, :, :, i, 0] = trans.resize(io.imread(training_set_dir + training_files[ind][0]), [img_size[0], img_size[1])
            labels[i] = training_files[ind][1]
    else:
        num_transforms = len(transforms)
        stack_of_images = np.zeros((img_size[0], img_size[1], 3, num_images, num_transforms + 1))
        labels = [''] * (num_images * (num_transforms + 1))
        for i, ind in zip(range(num_images), indices):
            Img = io.imread(training_set_dir + training_files[ind][0])
            stack_of_images[:, :, :, i, 0] = trans.resize(Img, [img_size[0], img_size[1]])
            labels[i] = training_files[ind][1]
            for ti, transform in zip(xrange(num_transforms), transforms):
                print transform
	        stack_of_images[:, :, :, i, ti+1] = transform_image(io.imread(training_set_dir + training_files[ind][0]), [img_size[0], img_size[1]], transform)
                labels[(ti+1)*num_images + i] = training_files[ind][1]

    return stack_of_images, labels

def transform_image(Img, img_size, transform):
    """
        Performs one of ROTATEX, HUE, CONTRAST.
        We also do the resizing after the transformation to minimize information loss.
    """

    if transform == 'ROTATE90':
        Img = trans.resize(trans.rotate(Img, 90, resize = True), [img_size[0], img_size[1]])
        return Img
    elif transform == 'ROTATE180':
        Img = trans.resize(trans.rotate(Img, 180, resize = True), [img_size[0], img_size[1]])
        return Img
    elif transform == 'ROTATE270':
        Img = trans.resize(trans.rotate(Img, 270, resize = True), [img_size[0], img_size[1]])
        return Img

    elif transform == 'HUE':
        Img = trans.resize(shift_hue(Img, 10), [img_size[0], img_size[1]])      ### change this to whatever shift you want: (-H, H).
        return Img

    elif transform == 'CONTRAST':
        Img = trans.resize(change_contrast(Img, [25, 400], [7, 14], 10), [img_size[0], img_size[1]])
        return Img
        

#Images, Labels = get_batch(num_images = 10, transforms = None, img_size = [500, 330], start_index = 0, random_shuffle = False)

Images, Labels = get_batch(num_images = 1, transforms = ['ROTATE270', 'HUE', 'CONTRAST'], img_size = [500, 330], start_index = 0, random_shuffle = False)

for ti in xrange(4):
    plt.imshow(Images[:, :, :, 0, ti])
    plt.show()

