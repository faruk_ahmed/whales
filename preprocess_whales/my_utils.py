from random import randint
from skimage import color
import numpy as np

def shift_hue(arr,hout):
    hsv = color.rgb2hsv(arr)
    hsv[...,0] = hsv[...,0] + 0.01*randint(-hout, hout)
    return color.hsv2rgb(hsv)

def change_contrast(arr,pow_range,prod_range,hout):
    hsv = color.rgb2hsv(arr)
    hsv[...,1] = np.power(hsv[...,1], 0.01*randint(pow_range[0], pow_range[1])) * 0.01*randint(prod_range[0], prod_range[1]) + 0.01*randint(-hout, hout)
    hsv[...,2] = np.power(hsv[...,2], 0.01*randint(pow_range[0], pow_range[1])) * 0.01*randint(prod_range[0], prod_range[1]) + 0.01*randint(-hout, hout)
    return color.hsv2rgb(arr)



