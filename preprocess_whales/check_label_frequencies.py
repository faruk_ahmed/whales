import numpy as np
import scipy 

import skimage
from skimage import transform as trans
import skimage.io as io

import matplotlib.pyplot as plt
import matplotlib
import pdb

import csv
from my_utils import *

matplotlib.use('Agg')


f = csv.reader(open('/data/lisatmp4/whales/data/train.csv'))
    
first_time = 1
unique_whales = {}
for row in f:
    if first_time:
        first_time = 0
        continue
    whale_id = row[1]
    if whale_id not in unique_whales:
        unique_whales[whale_id] = 1
    else:
        unique_whales[whale_id] += 1

np.save('unique_whales.npy', unique_whales)


pdb.set_trace()
