import numpy as np
import scipy 

import skimage
from skimage import transform as trans
import skimage.io as io

import matplotlib.pyplot as plt
import matplotlib
import pdb

import csv
from my_utils import *

matplotlib.use('Agg')


training_set_dir = '/data/lisatmp4/whales/data/imgs/'
f = csv.reader(open('/data/lisatmp4/whales/data/train.csv'))
    
training_files = []
first_time = 1
for row in f:
    if first_time:
        first_time = 0
        continue
    training_files.append((row[0], row[1]))

total_images = len(training_files)
aspect_ratios = []
indices = np.arange(total_images)

for i in indices:
    try:
        img = io.imread(training_set_dir + training_files[i][0])
        R = img.shape[0]
        C = img.shape[1]
        aspect_ratios.append(1. * R/C)
    except Exception, e:
        print training_files[i][0], str(e)

print 'Aspect ratio =', np.mean(aspect_ratios)
