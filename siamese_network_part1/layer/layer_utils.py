__author__ = 'KimTS'
from theano import tensor
from collections import OrderedDict
from utils.utils import secure_pickle_dump, get_tensor_params_val


def get_tensor_output(input, layers, is_training=True):
    output = input
    for layer in layers:
        output = layer.forward(output, is_training=is_training)
    return output

def get_model_updates(layers, cost, optimizer):
    updates_dict = OrderedDict()
    for layer in layers:
        if hasattr(layer, 'get_updates'):
            updates_dict = layer.get_updates(cost=cost, updates_dict=updates_dict, optimizer=optimizer)
    return updates_dict

def get_model_gradients(layers, cost):
    gradient_dict = OrderedDict()
    for layer in layers:
        if hasattr(layer, 'get_gradients'):
            gradient_dict = layer.get_gradients(cost=cost, gradients_dict=gradient_dict)
    return gradient_dict

def get_model_tensor_params(layers):
    tensor_params_dict = OrderedDict()

    for layer in layers:
        if hasattr(layer, 'get_tensor_params'):
            tensor_params_dict = layer.get_tensor_params(tensor_params_dict=tensor_params_dict)

    return tensor_params_dict

def load_model_tensor_params(layers, params_dict):
    for layer in layers:
        if hasattr(layer, 'set_tensor_params'):
            print 'load layer : ', layer.name
            layer.set_tensor_params(params_dicts=params_dict)

def save_model_params(layers, save_to):

    tensor_params_dict = get_model_tensor_params(layers)
    params_val_dict = get_tensor_params_val(tensor_params_dict)
    secure_pickle_dump(params_val_dict, save_to)

def get_L2_weight_decay(layers, weight_types=('W', 'b')):
    params_dict = OrderedDict()
    for layer in layers:
        if hasattr(layer, 'get_tensor_params'):
            params_dict = layer.get_tensor_params(param_types=weight_types, tensor_params_dict=params_dict)

    L2_weight_decay = 0.
    for param_name, param_tensor in params_dict.iteritems():
        L2_weight_decay += tensor.sum(param_tensor**2)

    return L2_weight_decay

def get_L1_weight_decay(layers, weight_types=('W', 'b')):
    params_dict = OrderedDict()
    for layer in layers:
        if hasattr(layer, 'get_tensor_params'):
            params_dict = layer.get_tensor_params(param_types=weight_types, tensor_params_dict=params_dict)

    L1_weight_decay = 0.
    for params in params_dict.values():
        L1_weight_decay += tensor.sum(abs(params))

    return L1_weight_decay