from layer.initializer import *
from utils.pad import pad
from utils.utils import prefix_name, get_tensor_params_val, save_params_val
from theano import tensor
from theano.tensor.nnet.conv import conv2d
from theano.tensor.signal.downsample import max_pool_2d
from theano.sandbox.rng_mrg import MRG_RandomStreams
import collections
from collections import OrderedDict

class RandomLayer(object):
    seed_rng = np.random.RandomState(1)
    def __init__(self):
        self.theano_seed = self.seed_rng.randint(np.iinfo(np.int32).max)
        self.theano_rng = MRG_RandomStreams(self.theano_seed)

class ParamLayer(object):
    def __init__(self,
                 name='param_layer'):
        self.name = name
        self.tensor_params_dict = OrderedDict()

    def init_params(self):
        NotImplementedError('initialize parameters')

    def get_tensor_param(self, param_type, tensor_params_dict=None):
        if tensor_params_dict is None:
            tensor_params_dict = OrderedDict()
        param_name = prefix_name(self.name, param_type)
        if param_name in self.tensor_params_dict:
            if param_name in tensor_params_dict:
                Warning('Already in dictionary')
            else:
                tensor_params_dict[param_name] = self.tensor_params_dict[param_name]
        else:
            Warning('Not available')

        return tensor_params_dict

    def get_tensor_params(self, param_types=('W', 'b'), tensor_params_dict=None):
        if tensor_params_dict is None:
            tensor_params_dict = OrderedDict()

        if param_types is None:
            for tensor_name, tensor_param in self.tensor_params_dict.iteritems():
                if tensor_name in tensor_params_dict:
                    Warning('Already in dictionary')
                else:
                    tensor_params_dict[tensor_name] = tensor_param
        else:
            if isinstance(param_types, collections.Iterable):
                for param_type in param_types:
                    tensor_params_dict = self.get_tensor_param(param_type, tensor_params_dict)
            else:
                tensor_params_dict = self.get_tensor_param(param_types, tensor_params_dict)
        return tensor_params_dict

    def set_tensor_params(self, params_dicts, param_type=None):
        if param_type is None:
            for tensor_name, tensor_param in self.tensor_params_dict.iteritems():
                if tensor_name in params_dicts:
                    param_value = params_dicts[tensor_name]
                    self.tensor_params_dict[tensor_name].set_value(param_value.astype(floatX))
                else:
                    Warning('Not available')
        else:
            tensor_name = prefix_name(self.name, param_type)
            if tensor_name in self.tensor_params_dict:
                if tensor_name in params_dicts:
                    param_value = params_dicts[tensor_name]
                    self.tensor_params_dict[tensor_name].set_value(param_value.astype(floatX))
                else:
                    Warning('Not available')
            else:
                Warning('Not available')

    def get_updates(self, cost, updates_dict=None, optimizer=None):
        if updates_dict is None:
            updates_dict = OrderedDict()
        for tensor_name, tensor_param in self.tensor_params_dict.iteritems():
            tensor_grad = tensor.grad(cost=cost, wrt=tensor_param)
            for param, update in optimizer(tensor_param, tensor_grad).iteritems():
                updates_dict[param] = update
        return updates_dict

    def get_gradients(self, cost, gradients_dict=None):
        if gradients_dict is None:
            gradients_dict = OrderedDict()
        for tensor_name, tensor_param in self.tensor_params_dict.iteritems():
            tensor_grad = tensor.grad(cost=cost, wrt=tensor_param, disconnected_inputs='ignore')
            gradients_dict[tensor_param] = tensor_grad
        return gradients_dict


class LinearLayer(ParamLayer):
    def __init__(self,
                 input_dim,
                 output_dim,
                 name='linear_layer',
                 **kwargs):
        super(LinearLayer, self).__init__(**kwargs)
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.name = name

        self.init_params()

    def init_params(self):
        params_dict = OrderedDict()
        params_dict[prefix_name(self.name, 'W')] = normal_weight(self.input_dim, self.output_dim)
        params_dict[prefix_name(self.name, 'b')] = np.zeros(shape=(self.output_dim,), dtype=floatX)
        self.tensor_params_dict = OrderedDict()
        for param_name, param_value in params_dict.iteritems():
            self.tensor_params_dict[param_name] = theano.shared(param_value, name=param_name)

    def forward(self, input_data, is_training=True):
        weight_param   = self.tensor_params_dict[prefix_name(self.name, 'W')]
        bias_param     = self.tensor_params_dict[prefix_name(self.name, 'b')]
        output_data    = tensor.dot(input_data, weight_param) + bias_param
        return output_data

class ConvolutionalLayer(ParamLayer):
    """
    TODO : NON TIED BIAS?
    """
    conv2d_impl = staticmethod(conv2d)
    def __init__(self,
                 input_size,
                 num_input_channels,
                 filter_size,
                 num_filters,
                 batch_size,
                 step_size=(1,1),
                 pad_size=(0,0),
                 name='conv_layer',
                 **kwargs):
        super(ConvolutionalLayer, self).__init__(**kwargs)
        self.input_size         = input_size
        self.num_input_channels = num_input_channels
        self.num_filters        = num_filters
        self.filter_size        = filter_size
        self.step_size          = step_size
        self.batch_size         = batch_size
        self.pad_size           = pad_size
        self.name               = name

        self.init_params()

    def init_params(self):
        params_dict = OrderedDict()
        params_dict[prefix_name(self.name, 'W')] = uniform_filter(self.num_input_channels, self.num_filters, self.filter_size)
        params_dict[prefix_name(self.name, 'b')] = np.zeros((self.num_filters,),dtype=floatX)
        self.tensor_params_dict = OrderedDict()
        for param_name, param_value in params_dict.iteritems():
            self.tensor_params_dict[param_name] = theano.shared(param_value, name=param_name)

    def forward(self, input_data, is_training=True):
        weight_param   = self.tensor_params_dict[prefix_name(self.name, 'W')]
        bias_param     = self.tensor_params_dict[prefix_name(self.name, 'b')]

        input_shape = (self.batch_size, self.num_input_channels,) + self.input_size
        if self.pad_size[0]!=0 or self.pad_size[1]!=0:
            padding = [(self.pad_size[0], self.pad_size[0]), (self.pad_size[1], self.pad_size[1])]
            input_data = pad(input_data, padding, batch_ndim=2)
            input_shape = (self.batch_size, self.num_input_channels,) + \
                          (self.input_size[0]+self.pad_size[0]*2, self.input_size[1]+self.pad_size[1]*2)

        output_data = self.conv2d_impl(input=input_data,
                                       filters=weight_param,
                                       image_shape=input_shape,
                                       subsample=self.step_size,
                                       border_mode='valid',
                                       filter_shape=((self.num_filters, self.num_input_channels) + self.filter_size))

        output_data += bias_param.dimshuffle('x', 0, 'x', 'x')
        return output_data

class MaxPoolLayer(object):
    def __init__(self,
                 input_size,
                 num_input_channels,
                 pool_size,
                 step_size=None,
                 name='maxpool_layer'):
        self.input_size         = input_size
        self.num_input_channels = num_input_channels
        self.input_shape        = (self.num_input_channels,)+self.input_size
        self.pool_size          = pool_size
        self.step_size          = step_size
        self.name               = name

    def forward(self, input_data, is_training=True):
        output_data = max_pool_2d(input=input_data,
                                  ds=self.pool_size,
                                  st=None,
                                  mode='max',
                                  ignore_border=True)
        return output_data

class UpPoolLayer(object):
    def __init__(self,
                 input_size,
                 num_input_channels,
                 pool_size=(2,2),
                 name='maxpool_layer'):
        self.input_size         = input_size
        self.num_input_channels = num_input_channels
        self.input_shape        = (self.num_input_channels,)+self.input_size
        self.pool_size          = pool_size
        self.name               = name

    def forward(self, input_data, is_training=True):
        output_data = input_data
        if self.pool_size[1] > 1:
            output_data = tensor.extra_ops.repeat(output_data, self.pool_size[1], 3)
        if self.pool_size[0] > 1:
            output_data = tensor.extra_ops.repeat(output_data, self.pool_size[0], 2)
        return output_data

class LinearBatchNormalization(ParamLayer):
    def __init__(self,
                 input_dim,
                 use_scale=False,
                 momentum=0.5,
                 eps=1e-20,
                 name='linear_bn_layer',
                 **kwargs):
        super(LinearBatchNormalization, self).__init__(**kwargs)
        self.input_dim          = input_dim
        self.use_scale          = use_scale
        self.momentum           = momentum
        self.eps                = eps
        self.name               = name
        self.updates_dict       = OrderedDict()

        self.init_params()

    def init_params(self):
        params_dict = OrderedDict()
        if self.use_scale:
            params_dict[prefix_name(self.name, 'W')]    = np.ones(shape=(self.input_dim,), dtype=floatX)
        params_dict[prefix_name(self.name, 'b')]    = np.zeros(shape=(self.input_dim,), dtype=floatX)
        params_dict[prefix_name(self.name, 'mean')] = np.zeros(shape=(self.input_dim,), dtype=floatX)
        params_dict[prefix_name(self.name, 'std')]  = np.ones(shape=(self.input_dim,), dtype=floatX)
        self.tensor_params_dict = OrderedDict()
        for param_name, param_value in params_dict.iteritems():
            self.tensor_params_dict[param_name] = theano.shared(param_value, name=param_name)

    def forward(self, input_data, is_training=True):
        bias_param     = self.tensor_params_dict[prefix_name(self.name, 'b')]

        if is_training:
            input_mean = input_data.mean(axis=0)
            input_std  = input_data.std(axis=0)

            output_data = (input_data-input_mean.dimshuffle('x', 0))/(input_std.dimshuffle('x', 0)+self.eps)
            if self.use_scale:
                weight_param   = self.tensor_params_dict[prefix_name(self.name, 'W')]
                output_data = output_data*weight_param.dimshuffle('x', 0)
            output_data = output_data + bias_param.dimshuffle('x', 0)

            mean_param  = self.tensor_params_dict[prefix_name(self.name, 'mean')]
            std_param   = self.tensor_params_dict[prefix_name(self.name, 'std')]

            self.updates_dict[mean_param] = (1.-self.momentum)*mean_param + self.momentum*input_mean
            self.updates_dict[std_param ] = (1.-self.momentum)*std_param  + self.momentum*input_std
        else:
            mean_param  = self.tensor_params_dict[prefix_name(self.name, 'mean')]
            std_param   = self.tensor_params_dict[prefix_name(self.name, 'std')]

            output_data = (input_data-mean_param.dimshuffle('x', 0))/(std_param.dimshuffle('x', 0)+self.eps)
            if self.use_scale:
                weight_param   = self.tensor_params_dict[prefix_name(self.name, 'W')]
                output_data = output_data*weight_param.dimshuffle('x', 0)
            output_data = output_data + bias_param.dimshuffle('x', 0)
        return output_data

    def get_updates(self, cost, updates_dict=None, optimizer=None):
        if updates_dict is None:
            updates_dict = OrderedDict()
        # updates for W
        if self.use_scale:
            tensor_weight_param  = self.tensor_params_dict[prefix_name(self.name, 'W')]
            tensor_weight_grad   = tensor.grad(cost=cost, wrt=tensor_weight_param)
            for param, update in optimizer(tensor_weight_param, tensor_weight_grad).iteritems():
                updates_dict[param] = update

        # updates for b
        tensor_bias_param  = self.tensor_params_dict[prefix_name(self.name, 'b')]
        tensor_bias_grad   = tensor.grad(cost=cost, wrt=tensor_bias_param)
        for param, update in optimizer(tensor_bias_param, tensor_bias_grad).iteritems():
            updates_dict[param] = update

        for param, update in self.updates_dict.iteritems():
            updates_dict[param] = update
        return updates_dict


class ConvBatchNormalization(ParamLayer):
    def __init__(self,
                 input_size,
                 num_input_channels,
                 use_scale=False,
                 momentum=0.5,
                 eps=0.01,
                 name='conv_bn_layer',
                 **kwargs):
        super(ConvBatchNormalization, self).__init__(**kwargs)
        self.input_size         = input_size
        self.num_input_channels = num_input_channels
        self.use_scale          = use_scale
        self.input_shape        = (self.num_input_channels,)+self.input_size
        self.momentum           = momentum
        self.eps                = eps
        self.name               = name
        self.updates_dict       = OrderedDict()

        self.init_params()

    def init_params(self):
        params_dict = OrderedDict()
        if self.use_scale:
            params_dict[prefix_name(self.name, 'W')]    = np.ones(shape=(self.num_input_channels,), dtype=floatX)
        params_dict[prefix_name(self.name, 'b')]    = np.zeros(shape=(self.num_input_channels,), dtype=floatX)
        params_dict[prefix_name(self.name, 'mean')] = np.zeros(shape=(self.num_input_channels,), dtype=floatX)
        params_dict[prefix_name(self.name, 'std')]  = np.ones(shape=(self.num_input_channels,), dtype=floatX)
        self.tensor_params_dict = OrderedDict()
        for param_name, param_value in params_dict.iteritems():
            self.tensor_params_dict[param_name] = theano.shared(param_value, name=param_name)

    def forward(self, input_data, is_training=True):
        bias_param     = self.tensor_params_dict[prefix_name(self.name, 'b')]

        if is_training:
            input_mean = input_data.mean(axis=(0,2,3))
            input_std  = input_data.std(axis=(0,2,3))

            output_data = (input_data-input_mean.dimshuffle('x', 0, 'x', 'x'))/(input_std.dimshuffle('x', 0, 'x', 'x')+self.eps)
            if self.use_scale:
                weight_param   = self.tensor_params_dict[prefix_name(self.name, 'W')]
                output_data = output_data*weight_param.dimshuffle('x', 0, 'x', 'x')
            output_data += bias_param.dimshuffle('x', 0, 'x', 'x')

            mean_param  = self.tensor_params_dict[prefix_name(self.name, 'mean')]
            std_param   = self.tensor_params_dict[prefix_name(self.name, 'std')]
            self.updates_dict[mean_param] = (1.-self.momentum)*mean_param + self.momentum*input_mean
            self.updates_dict[std_param ] = (1.-self.momentum)*std_param  + self.momentum*input_std
        else:
            mean_param  = self.tensor_params_dict[prefix_name(self.name, 'mean')]
            std_param   = self.tensor_params_dict[prefix_name(self.name, 'std')]

            output_data = (input_data-mean_param.dimshuffle('x', 0, 'x', 'x'))/(std_param.dimshuffle('x', 0, 'x', 'x')+self.eps)
            if self.use_scale:
                weight_param   = self.tensor_params_dict[prefix_name(self.name, 'W')]
                output_data = output_data*weight_param.dimshuffle('x', 0, 'x', 'x')
            output_data += bias_param.dimshuffle('x', 0, 'x', 'x')
        return output_data

    def get_updates(self, cost, updates_dict=None, optimizer=None):
        if updates_dict is None:
            updates_dict = OrderedDict()
        # updates for W
        if self.use_scale:
            tensor_weight_param  = self.tensor_params_dict[prefix_name(self.name, 'W')]
            tensor_weight_grad   = tensor.grad(cost=cost, wrt=tensor_weight_param)
            for param, update in optimizer(tensor_weight_param, tensor_weight_grad).iteritems():
                updates_dict[param] = update

        # updates for b
        tensor_bias_param  = self.tensor_params_dict[prefix_name(self.name, 'b')]
        tensor_bias_grad   = tensor.grad(cost=cost, wrt=tensor_bias_param)
        for param, update in optimizer(tensor_bias_param, tensor_bias_grad).iteritems():
            updates_dict[param] = update

        for param, update in self.updates_dict.iteritems():
            updates_dict[param] = update
        return updates_dict

class DropoutLayer(RandomLayer):
    def __init__(self,
                 ratio=0.5,
                 name='drop_layer'):
        super(DropoutLayer, self).__init__()
        self.ratio = ratio
        self.name  = name

    def forward(self, input_data, is_training=True):
        if is_training:
            dropout_mask = self.theano_rng.binomial(size=input_data.shape, p=self.ratio, dtype=floatX)
        else:
            dropout_mask = (1.-self.ratio)
        output_data = input_data*dropout_mask
        return output_data

class FlatteningLayer(object):
    def __init__(self,
                 name='flat_layer'):
        self.name = name
    def forward(self, input_data, is_training):
        return input_data.flatten(ndim=2)

class ReshapingLayer(object):
    def __init__(self,
                 shape,
                 name='reshape_layer'):
        self.shape = shape
        self.name  = name
    def forward(self, input_data, is_training):
        return input_data.reshape((input_data.shape[0],) + self.shape)
class NormalRandomLayer(RandomLayer):
    def __init__(self,
                 input_shape,
                 avg=0.0,
                 std=1.0,
                 name='normal_rand_layer'):
        super(NormalRandomLayer, self).__init__()
        self.input_shape = input_shape
        self.avg         = avg
        self.std         = std
        self.name        = name

    def forward(self, batch_size, is_training=True):
        output_data = self.theano_rng.normal(size=(batch_size,) + self.input_shape, avg=self.avg, std=self.std, dtype=floatX)
        return output_data

class UniformRandomLayer(RandomLayer):
    def __init__(self,
                 input_shape,
                 low=-1.0,
                 high=1.0,
                 name='uniform_rand_layer'):
        super(UniformRandomLayer, self).__init__()
        self.input_shape = input_shape
        self.low         = low
        self.high        = high
        self.name        = name

    def forward(self, batch_size, is_training=True):
        output_data = self.theano_rng.uniform(size=(batch_size,) + self.input_shape, low=self.low, high=self.high, dtype=floatX)
        return output_data


class InjectNormalRandomLayer(RandomLayer):
    def __init__(self,
                 avg=0.0,
                 std=1.0,
                 name='inject_normal_rand_layer'):
        super(InjectNormalRandomLayer, self).__init__()
        self.avg         = avg
        self.std         = std
        self.name        = name

    def forward(self, input_data, is_training=True):
        noise_data  = self.theano_rng.normal(size=input_data.shape, avg=self.avg, std=self.std, dtype=floatX)
        output_data = input_data + noise_data
        return output_data

class RecurrentLayer(ParamLayer):
    def __init__(self,
                 input_dim,
                 hidden_dim,
                 init_hidden=None,
                 name='recurrent_layer',
                 **kwargs):
        super(RecurrentLayer, self).__init__(**kwargs)
        self.input_dim   = input_dim
        self.hidden_dim  = hidden_dim
        self.init_hidden =init_hidden
        self.name        = name


    def init_params(self):
        params_dict = OrderedDict()
        params_dict[prefix_name(self.name, 'W_input_to_hidden')]  = normal_weight(self.input_dim, self.hidden_dim)
        params_dict[prefix_name(self.name, 'W_hidden_to_hidden')] = orthogonal_weight(self.hidden_dim)
        params_dict[prefix_name(self.name, 'b_hidden')] = np.zeros(shape=(self.hidden_dim,), dtype=floatX)
        self.tensor_params_dict = OrderedDict()
        for param_name, param_value in params_dict.iteritems():
            self.tensor_params_dict[param_name] = theano.shared(param_value, name=param_name)

    def step_function(self, input_mask, input_to_hidden, prev_hidden_data):
        W_hidden_to_hidden = self.tensor_params_dict[prefix_name(self.name, 'W_hidden_to_hidden')]
        b_hidden           = self.tensor_params_dict[prefix_name(self.name, 'b_hidden')]

        pre_activation = tensor.dot(prev_hidden_data, W_hidden_to_hidden) + input_to_hidden + b_hidden
        hidden_data    = tensor.tanh(pre_activation)
        hidden_data    = input_mask.dimshuffle(0,'x')*hidden_data + (1.0-input_mask.dimshuffle(0,'x'))*prev_hidden_data
        return hidden_data

    def forward(self, input_data, input_mask=None, is_training=True):
        num_steps    = input_data.shape[0]
        num_samples  = input_data.shape[1]

        if input_mask is None:
            input_mask = tensor.alloc(1.0, num_steps, 1)
        if self.init_hidden is None:
            self.init_hidden = tensor.alloc(0.0, num_samples, self.hidden_dim)

        W_input_to_hidden  = self.tensor_params_dict[prefix_name(self.name, 'W_input_to_hidden')]
        input_to_hidden    = tensor.dot(input_data, W_input_to_hidden)

        hidden_data, updates = theano.scan(fn=self.step_function,
                                           sequences=[input_mask, input_to_hidden],
                                           outputs_info=[self.init_hidden])
        return hidden_data


class GruLayer(ParamLayer):
    def __init__(self,
                 input_dim,
                 hidden_dim,
                 name='recurrent_layer',
                 **kwargs):
        super(GruLayer, self).__init__(**kwargs)
        self.input_dim  = input_dim
        self.hidden_dim = hidden_dim
        self.name       = name

    def init_params(self):
        params_dict = OrderedDict()
        # update gate
        params_dict[prefix_name(self.name, 'W_input_to_update')]  = normal_weight(self.input_dim, self.hidden_dim)
        params_dict[prefix_name(self.name, 'W_hidden_to_update')] = orthogonal_weight(self.hidden_dim)
        params_dict[prefix_name(self.name, 'b_update')]           = np.zeros((self.hidden_dim,), dtype=floatX)

        # reset gate
        params_dict[prefix_name(self.name, 'W_input_to_reset')]   = normal_weight(self.input_dim, self.hidden_dim)
        params_dict[prefix_name(self.name, 'W_hidden_to_reset')]  = orthogonal_weight(self.hidden_dim)
        params_dict[prefix_name(self.name, 'b_reset')]            = np.zeros((self.hidden_dim,), dtype=floatX)

        # output
        params_dict[prefix_name(self.name, 'W_input_to_hidden')]  = normal_weight(self.input_dim, self.hidden_dim)
        params_dict[prefix_name(self.name, 'W_hidden_to_hidden')] = orthogonal_weight(self.hidden_dim)
        params_dict[prefix_name(self.name, 'b_hidden')]           = np.zeros((self.hidden_dim,), dtype=floatX)

        self.tensor_params_dict = OrderedDict()
        for param_name, param_value in params_dict.iteritems():
            self.tensor_params_dict[param_name] = theano.shared(param_value, name=param_name)

    def step_function(self, input_mask, input_to_update, input_to_reset, input_to_hidden, prev_hidden_data):
        W_hidden_to_update    = self.tensor_params_dict[prefix_name(self.name, 'W_hidden_to_update')]
        b_update              = self.tensor_params_dict[prefix_name(self.name, 'b_update')]
        pre_activation_update = tensor.dot(prev_hidden_data, W_hidden_to_update) + input_to_update + b_update
        update                = tensor.nnet.sigmoid(pre_activation_update)

        W_hidden_to_reset     = self.tensor_params_dict[prefix_name(self.name, 'W_hidden_to_reset')]
        b_reset               = self.tensor_params_dict[prefix_name(self.name, 'b_reset')]
        pre_activation_reset  = tensor.dot(prev_hidden_data, W_hidden_to_reset) + input_to_reset + b_reset
        reset                 = tensor.nnet.sigmoid(pre_activation_reset)

        W_hidden_to_hidden    = self.tensor_params_dict[prefix_name(self.name, 'W_hidden_to_hidden')]
        b_hidden              = self.tensor_params_dict[prefix_name(self.name, 'b_hidden')]
        pre_activation_hidden = tensor.dot(reset*prev_hidden_data, W_hidden_to_hidden) + input_to_hidden + b_hidden

        hidden_data = update*prev_hidden_data + (1.0-update)*tensor.tanh(pre_activation_hidden)
        hidden_data = input_mask.dimshuffle(0,'x')*hidden_data + (1.0-input_mask.dimshuffle(0,'x'))*prev_hidden_data

        return hidden_data

    def forward(self, input_data, input_mask=None, init_state=None, is_training=True):
        num_steps    = input_data.shape[0]
        num_samples  = input_data.shape[1]

        if input_mask is None:
            input_mask = tensor.alloc(1.0, num_steps, 1)
        if init_state is None:
            init_state = tensor.alloc(0.0, num_samples, self.hidden_dim)

        W_input_to_update  = self.tensor_params_dict[prefix_name(self.name, 'W_input_to_update')]
        W_input_to_reset   = self.tensor_params_dict[prefix_name(self.name, 'W_input_to_reset')]
        W_input_to_hidden  = self.tensor_params_dict[prefix_name(self.name, 'W_input_to_hidden')]
        input_to_update    = tensor.dot(input_data, W_input_to_update)
        input_to_reset     = tensor.dot(input_data, W_input_to_reset)
        input_to_hidden    = tensor.dot(input_data, W_input_to_hidden)

        hidden_data, updates = theano.scan(fn=self.step_function,
                                           sequences=[input_mask, input_to_update, input_to_reset, input_to_hidden],
                                           outputs_info=[init_state])
        return hidden_data
#
# class LstmLayer(ParamLayer):
#     def __init__(self):

