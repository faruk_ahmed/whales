__author__ = 'KimTS'
from theano import tensor
class Tanh(object):
    def __init__(self,
                 name='tanh_layer'):
        self.name = name
    def forward(self, input_data, is_training=True):
        return tensor.tanh(input_data)

class Relu(object):
    def __init__(self,
                 name='relu_layer'):
        self.name = name
    def forward(self, input_data, is_training=True):
        return tensor.maximum(0.0, input_data)

class Logistic(object):
    def __init__(self,
                 name='logit_layer'):
        self.name = name
    def forward(self, input_data, is_training=True):
        return tensor.nnet.sigmoid(input_data)

class LeakyRelu(object):
    def __init__(self,
                 slope=0.01,
                 name='leakrelu_layer'):
        self.name = name
    def forward(self, input_data, is_training=True):
        return tensor.maximum(0.01*input_data, input_data)