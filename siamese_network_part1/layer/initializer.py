import numpy as np
import theano

floatX = theano.config.floatX
def orthogonal_weight(num_dims):
    init_val = np.random.randn(num_dims, num_dims)
    u, s, v  = np.linalg.svd(init_val)
    return  u

def normal_weight(input_dim, output_dim, scale=0.01):
    init_val= np.random.randn(input_dim, output_dim).astype(floatX)
    return scale*init_val

def normal_filter(num_in_channels, num_out_channels, filter_size, scale=1.0):
    init_val= np.random.randn(num_out_channels, num_in_channels, filter_size[0], filter_size[1]).astype(floatX)
    return scale*init_val

def uniform_filter(num_in_channels, num_out_channels, filter_size):
    shape = (num_out_channels, num_in_channels, filter_size[0], filter_size[1])
    width = 2./(np.sqrt(num_in_channels*filter_size[0]*filter_size[1]))
    init_val = np.random.uniform(low=-width, high=width, size=shape).astype(floatX)
    return init_val
