from theano import tensor

def binary_crossentropy(predictions, targets):
    """
    L = -t*log(p)-(1-t)*log(1-p)
    """
    return tensor.nnet.binary_crossentropy(predictions, targets)