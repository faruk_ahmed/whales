__author__ = 'KimTS'
from theano import tensor

def mean_square_error(predictions, targets):

    return tensor.sum((predictions-targets)**2)