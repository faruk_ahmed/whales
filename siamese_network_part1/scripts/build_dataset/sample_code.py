__author__ = 'KimTS'


# open HDF5 file for writing and create three datasets in root group

import h5py
import numpy
from fuel.datasets.hdf5 import H5PYDataset

def build_fuel_hdf5(hdf5_filepath = 'test_path'):
    train_vector_features = []
    test_vector_features  = []
    train_image_features  = []
    test_image_features   = []
    train_targets         = []
    test_targets          = []

    with h5py.File(hdf5_filepath, mode='w') as h5file:
        vector_feature = h5file.create_dataset(name='vector_feat', shape=(100, 10), dtype='float32')
        image_feature  = h5file.create_dataset(name='image_feat' , shape=(100, 3, 5, 5), dtype='uint8')
        target_feature = h5file.create_dataset(name='target_feat', shape=(100, 1), dtype='uint8')

        # fill data into datasets
        vector_feature[...] = numpy.vstack([train_vector_features, test_vector_features])
        image_feature[...]  = numpy.vstack([train_image_features, test_image_features])
        target_feature[...] = numpy.vstack([train_targets, test_targets])

        # label each dataset axis
        vector_feature.dims[0].label = 'batch'
        vector_feature.dims[1].label = 'feature'
        image_feature.dims[0].label  = 'batch'
        image_feature.dims[1].label  = 'channel'
        image_feature.dims[2].label  = 'height'
        image_feature.dims[3].label  = 'width'
        target_feature.dims[0].label = 'batch'
        target_feature.dims[1].label = 'label'

        # set split attributes
        if 0:
            split_array = numpy.empty(shape=6,
                                      dtype=numpy.dtype([('split', 'a', 5),
                                                         ('source', 'a', 15),
                                                         ('start', numpy.int64, 1),
                                                         ('stop', numpy.int64, 1),
                                                         ('indices', h5py.special_dtype(ref=h5py.Reference)),
                                                         ('available', numpy.bool, 1),
                                                         ('comment', 'a', 1)]))
            split_array[0:3]['split'] = 'train'.encode('utf8')
            split_array[3:6]['split'] = 'test'.encode('utf8')
            split_array[0:6:3]['source'] = 'vector_feature'.encode('utf8')
            split_array[1:6:3]['source'] = 'image_feature'.encode('utf8')
            split_array[2:6:3]['source'] = 'target'.encode('utf8')
            split_array[0:3]['start'] = 0
            split_array[0:3]['stop'] = 90
            split_array[3:6]['start'] = 90
            split_array[3:6]['stop'] = 100
            split_array[:]['indices'] = h5py.Reference()
            split_array[:]['available'] = True
            split_array[:]['comment'] = '.'.encode('utf8')
            h5file.attrs['split'] = split_array
        else:
            split_dict = {'train': {'vector_features': ( 0,  90), 'image_features': ( 0,  90), 'targets': ( 0,  90)},
                          'test' : {'vector_features': (90, 100), 'image_features': (90, 100), 'targets': (90, 100)}}
            h5file.attrs['split'] = H5PYDataset.create_split_array(split_dict)

        h5file.flush()
        h5file.close()

def test_fuel_hdf5(hdf5_filepath = 'test_path'):
     train_set = H5PYDataset(file_or_path=hdf5_filepath, which_sets=('train',), subset=slice(0, 80))
     print train_set.num_examples
     print train_set.provides_sources
     print train_set.axis_labels[train_set.provides_sources[0]]
     print train_set.axis_labels[train_set.provides_sources[1]]
     print train_set.axis_labels[train_set.provides_sources[2]]