import theano
from theano import tensor
import numpy
import h5py
import tables
import cv2
from utils.image import crop_square_image, image_transform
from  collections import OrderedDict
from fuel.datasets.hdf5 import H5PYDataset
from sklearn_theano.feature_extraction import caffe


image_prefix = '/data/lisatmp4/taesup/whales/cropped_'

def build_label_dict(label_list):
    label_dict = OrderedDict()
    label_cnt  = 0
    for label in label_list:
        if label not in label_dict:
            label_dict[label]=label_cnt
            label_cnt+=1
    return label_dict

def get_data_list(data_split_file):
    with tables.openFile(data_split_file) as h5file:
        train_infos = h5file.root.train_files.read()
        valid_infos = h5file.root.val_files.read()
        test_infos  = h5file.root.test_files.read()
    train_label = []
    train_name  = []
    valid_label = []
    valid_name  = []
    test_name   = test_infos

    for train_info in train_infos:
        train_info = train_info.split('/')
        train_label.append(train_info[0])
        train_name.append(train_info[1])

    for valid_info in valid_infos:
        valid_info = valid_info.split('/')
        valid_label.append(valid_info[0])
        valid_name.append(valid_info[1])

    label_dict = build_label_dict(train_label+valid_label)

    for i, label in enumerate(train_label):
        label_idx = label_dict[label]
        train_label[i] = label_idx

    for i, label in enumerate(valid_label):
        label_idx = label_dict[label]
        valid_label[i] = label_idx

    return train_name, train_label, valid_name, valid_label, test_name, label_dict

# get pair list for training (balanced)
def get_pair_list(name_list, label_list):
    # convert label list into numpy array
    label_list = numpy.asarray(label_list)
    # get total number of unique labels
    num_labels = label_list.max()+1
    pair_image_idx = []
    pair_label     = []

    # for each label (same label pair)
    for label_idx in xrange(num_labels):
        # get sample index with current label
        sample_list = numpy.where(label_list==label_idx)[0]
        num_samples = len(sample_list)
        # more than one samples, make pair of them
        if num_samples>1:
            # to make all possible pairs
            for i in xrange(num_samples-1):
                for j in xrange(i+1, num_samples):
                    pair_idx = (name_list[sample_list[i]], name_list[sample_list[j]])
                    pair_image_idx.append(pair_idx)
                    pair_label.append(0)

    num_same_pair = len(pair_image_idx)

    # for each label (different label pair)
    num_rands = num_same_pair/num_labels+1
    for label_idx in xrange(num_labels):
        rands_cnt = 0
        while True:
            pair_idx =numpy.random.randint(0, num_labels,1)
            if pair_idx!=label_idx:
                pair_image_idx.append((name_list[label_idx], name_list[pair_idx]))
                pair_label.append(1)
                rands_cnt+=1
                if num_rands==rands_cnt:
                    break
        if len(pair_image_idx)>num_same_pair*2:
            break
    return pair_image_idx, pair_label

def get_feature_extractor(feature_layer_name, input_shape=(3, 224, 224)):
    print 'get theano expression'
    layers, input_var= caffe.googlenet.create_theano_expressions()
    print 'compile feature extractor'
    feature_extractor = theano.function([input_var], layers[feature_layer_name])

    # get feature size
    sample_data = numpy.zeros((1,)+input_shape, dtype=theano.config.floatX)
    sample_feat = feature_extractor(sample_data)

    return feature_extractor, sample_feat.shape[1:]

def build_pair_whale_fuel_hdf5(feature_layer_name,
                               data_split_file,
                               train_pair_hdf5_file,
                               train_hdf5_file,
                               valid_hdf5_file,
                               test_hdf5_file):

    # get feature extractor with output shape
    feature_extractor, feature_shape = get_feature_extractor(feature_layer_name, (3, 224, 224))

    # get data split for train, validation, test
    train_name, train_label, valid_name, valid_label, test_name, label_dict = get_data_list(data_split_file)

    # open hdf5 file for writing features (training pair)
    with h5py.File(train_pair_hdf5_file, mode='w') as h5file:
        # get pairs
        pair_names, pair_labels = get_pair_list(train_name, train_label)
        num_pairs = len(pair_names)

        # set vector feature, and target feature
        vector_feature = h5file.create_dataset(name='vector_feature', shape=(num_pairs,) + feature_shape, dtype='float32')
        target_feature = h5file.create_dataset(name='target_feature', shape=(num_pairs, 1), dtype='float32')

        # for each pair
        for i in xrange(num_pairs):
            print 'train pair feature : {}/{}'.format(i+1, num_pairs)

            # read current image, get label for it
            image_data0 = cv2.imread(image_prefix+pair_names[i][0])
            image_data0 = crop_square_image(image_data0, 224)
            image_data1 = cv2.imread(image_prefix+pair_names[i][1])
            image_data1 = crop_square_image(image_data1, 224)
            label_data  = pair_labels[i]

            # transpose
            image_data0 = image_data0.transpose(2,0,1)/255.-0.5
            image_data1 = image_data1.transpose(2,0,1)/255.-0.5
            image_data0 = image_data0.reshape((1,)+image_data0.shape)
            image_data1 = image_data1.reshape((1,)+image_data1.shape)

            # feature extraction
            feature_data0 = feature_extractor(image_data0.astype(theano.config.floatX))
            feature_data1 = feature_extractor(image_data1.astype(theano.config.floatX))

            # feature extraction/ save labels
            vector_feature[i] = feature_data0-feature_data1
            target_feature[i] = label_data

        # label each dataset axis
        vector_feature.dims[0].label = 'batch'
        vector_feature.dims[1].label = 'feature'
        target_feature.dims[0].label = 'batch'
        target_feature.dims[1].label = 'label'


        split_dict = {'train': {'vector_feature': ( 0,  num_pairs),
                                'target_feature': ( 0,  num_pairs)}}

        h5file.attrs['split'] = H5PYDataset.create_split_array(split_dict)

        h5file.flush()
        h5file.close()

    # open hdf5 file for writing features (validation)
    with h5py.File(valid_hdf5_file, mode='w') as h5file:
        num_images = len(valid_name)

        # set vector feature, and target feature
        vector_feature = h5file.create_dataset(name='vector_feature', shape=(num_images,) + feature_shape, dtype='float32')
        target_feature = h5file.create_dataset(name='target_feature', shape=(num_images, 1), dtype='float32')

        # for each pair
        for i in xrange(num_images):
            print 'valid feature : {}/{}'.format(i+1, num_images)

            # read current image, get label for it
            image_data = cv2.imread(image_prefix+valid_name[i])
            image_data = crop_square_image(image_data, 224)
            label_data = valid_label[i]

            # transpose
            image_data = image_data.transpose(2,0,1)/255.-0.5
            image_data = image_data.reshape((1,)+image_data.shape)

            # feature extraction
            feature_data = feature_extractor(image_data.astype(theano.config.floatX))

            # feature extraction/ save labels
            vector_feature[i] = feature_data
            target_feature[i] = label_data

        # label each dataset axis
        vector_feature.dims[0].label = 'batch'
        vector_feature.dims[1].label = 'feature'
        target_feature.dims[0].label = 'batch'
        target_feature.dims[1].label = 'label'


        split_dict = {'valid': {'vector_feature': ( 0,  num_images),
                                'target_feature': ( 0,  num_images)}}

        h5file.attrs['split'] = H5PYDataset.create_split_array(split_dict)

        h5file.flush()
        h5file.close()

    # open hdf5 file for writing features (test)
    with h5py.File(test_hdf5_file, mode='w') as h5file:
        num_images = len(test_name)

        # set vector feature, and target feature
        vector_feature = h5file.create_dataset(name='vector_feature', shape=(num_images,) + feature_shape, dtype='float32')
        name_feature   = h5file.create_dataset(name='name_feature', shape=(num_images,1), dtype='S10')

        # for each pair
        for i in xrange(num_images):
            print 'test feature : {}/{}'.format(i+1, num_images)

            # read current image, get label for it
            image_data = cv2.imread(image_prefix+test_name[i])
            image_data = crop_square_image(image_data, 224)
            label_data = -1

            # transpose
            image_data = image_data.transpose(2,0,1)/255.-0.5
            image_data = image_data.reshape((1,)+image_data.shape)

            # feature extraction
            feature_data = feature_extractor(image_data.astype(theano.config.floatX))

            # feature extraction/ save labels
            vector_feature[i] = feature_data
            name_feature[i]   = test_name[i]

        # label each dataset axis
        vector_feature.dims[0].label = 'batch'
        vector_feature.dims[1].label = 'feature'
        name_feature.dims[0].label = 'batch'
        name_feature.dims[1].label = 'name'


        split_dict = {'test': {'vector_feature': ( 0,  num_images), 'name_feature': ( 0,  num_images)}}

        h5file.attrs['split'] = H5PYDataset.create_split_array(split_dict)

        h5file.flush()
        h5file.close()

        import pickle
        pickle.dump( label_dict, open( "/data/lisatmp4/taesup/whale_label_dict.pkl", "wb" ) )

    # number of labels
    num_labels = max(train_label)+1

    train_label = numpy.asarray(train_label)

    # open hdf5 file for writing features
    with h5py.File(train_hdf5_file, mode='w') as h5file:
        # set vector feature, and target feature
        vector_feature = h5file.create_dataset(name='vector_feature', shape=(num_labels,) + feature_shape, dtype='float32')
        target_feature = h5file.create_dataset(name='target_feature', shape=(num_labels, 1), dtype='float32')

        # for train set
        for l in xrange(num_labels):
            print 'mean label :  {}/{}'.format((l+1),num_labels)

            # set start/end index
            start_idx  = l
            end_idx    = l+1

            image_idx_list = numpy.where(train_label==l)[0]

            mean_feature = 0.
            count = 0.
            for i in image_idx_list:
                # read current image, get label for it
                image_data = cv2.imread(image_prefix+train_name[i])

                # crop image
                image_data = crop_square_image(image_data, 224)

                # transpose
                image_data = image_data.transpose(2,0,1)/255.-0.5
                image_data = image_data.reshape((1,)+image_data.shape)
                # feature extraction/ save labels
                image_feature = feature_extractor(image_data.astype(theano.config.floatX))
                mean_feature += image_feature
                count += 1.
            vector_feature[start_idx:end_idx] = mean_feature/count
            target_feature[start_idx:end_idx] = l



        # label each dataset axis
        vector_feature.dims[0].label = 'batch'
        vector_feature.dims[1].label = 'feature'
        target_feature.dims[0].label = 'batch'
        target_feature.dims[1].label = 'label'

        split_dict = {'train': {'vector_feature': ( 0,  num_labels),
                                'target_feature': ( 0,  num_labels)}}
        h5file.attrs['split'] = H5PYDataset.create_split_array(split_dict)

        h5file.flush()
        h5file.close()



def build_single_whale_fuel_hdf5(feature_layer_name, data_split_file, hdf5_file, num_trans):

    # get feature extractor with output shape
    feature_extractor, feature_shape = get_feature_extractor(feature_layer_name, (3, 224, 224))

    # get data split for train, validation, test
    train_name, train_label, valid_name, valid_label, test_name, label_dict = get_data_list(data_split_file)

    # get size of data
    train_size = len(train_name)
    valid_size = len(valid_name)
    test_size  = len(test_name)

    print train_size, valid_size, test_size
    # get total data size
    total_num_data = train_size*num_trans + valid_size + test_size

    # open hdf5 file for writing features
    with h5py.File(hdf5_file, mode='w') as h5file:
        # set vector feature, and target feature
        vector_feature = h5file.create_dataset(name='vector_feature', shape=(total_num_data,) + feature_shape, dtype='float32')
        target_feature = h5file.create_dataset(name='target_feature', shape=(total_num_data, 1), dtype='uint8')

        # for train set
        for i in xrange(train_size):
            print 'feature : train {}/{}'.format((i+1)*num_trans,total_num_data)

            # set start/end index
            start_idx  = (i * num_trans)
            end_idx    = ((i + 1) * num_trans)

            # read current image, get label for it
            image_data = cv2.imread(image_prefix+train_name[i])
            label_data = train_label[i]

            # transform images
            image_data_set = image_transform(image_data,
                                             224,
                                             num_trans,
                                             0.8,
                                             1.2,
                                             30,
                                             (0.0, 0.2))
            # normalize
            image_data_set /= 255.
            # transpose
            image_data_set = image_data_set.transpose(0,3,1,2)
            # feature extraction/ save labels
            vector_feature[start_idx:end_idx] = feature_extractor(image_data_set.astype(theano.config.floatX))
            target_feature[start_idx:end_idx] = label_data

        # for valid set
        for i in xrange(valid_size):
            print 'feature : valid {}/{}'.format(train_size*num_trans+i+1,total_num_data)

            # set start/end index
            start_idx  = i+train_size*num_trans
            end_idx    = start_idx + 1

            # read image data
            image_data = cv2.imread(image_prefix+valid_name[i])
            label_data = valid_label[i]

            # crop image
            image_data = crop_square_image(image_data, 224)

            # transpose
            image_data = image_data.transpose(2,0,1)/255.
            image_data = image_data.reshape((1,)+image_data.shape)

            # feature extraction/ save labels
            vector_feature[start_idx:end_idx] = feature_extractor(image_data.astype(theano.config.floatX))
            target_feature[start_idx:end_idx] = label_data

        # for test set
        for i in xrange(test_size):
            print 'feature : test {}/{}'.format(train_size+valid_size+i+1,total_num_data)
            # set start/end index
            start_idx  = i+train_size*num_trans+valid_size
            end_idx    = start_idx + 1
            # read image data
            image_data = cv2.imread(image_prefix+test_name[i])
            label_data = -1

            # crop image
            image_data = crop_square_image(image_data, 224)

            # transpose
            image_data = image_data.transpose(2,0,1)/255.
            image_data = image_data.reshape((1,)+image_data.shape)

            # feature extraction/ save labels
            vector_feature[start_idx:end_idx] = feature_extractor(image_data.astype(theano.config.floatX))
            target_feature[start_idx:end_idx] = label_data

        # label each dataset axis
        vector_feature.dims[0].label = 'batch'
        vector_feature.dims[1].label = 'feature'
        target_feature.dims[0].label = 'batch'
        target_feature.dims[1].label = 'label'


        split_dict = {'train': {'vector_feature': ( 0,  train_size),
                                'target_feature': ( 0,  train_size)},
                      'valid': {'vector_feature': (train_size, train_size+valid_size),
                                'target_feature': (train_size, train_size+valid_size)},
                      'test' : {'vector_feature': (train_size+valid_size, train_size+valid_size+test_size),
                                'target_feature': (train_size+valid_size, train_size+valid_size+test_size)}}
        h5file.attrs['split'] = H5PYDataset.create_split_array(split_dict)

        h5file.flush()
        h5file.close()

def build_mean_whale_fuel_hdf5(feature_layer_name, data_split_file, train_mean_hdf5_file):

    # get feature extractor with output shape
    feature_extractor, feature_shape = get_feature_extractor(feature_layer_name, (3, 224, 224))

    # get data split for train, validation, test
    train_name, train_label, valid_name, valid_label, test_name, label_dict = get_data_list(data_split_file)

    # number of labels
    num_labels = max(train_label)+1

    train_label = numpy.asarray(train_label)

    # open hdf5 file for writing features
    with h5py.File(train_mean_hdf5_file, mode='w') as h5file:
        # set vector feature, and target feature
        vector_feature = h5file.create_dataset(name='vector_feature', shape=(num_labels,) + feature_shape, dtype='float32')
        target_feature = h5file.create_dataset(name='target_feature', shape=(num_labels, 1), dtype='float32')

        # for train set
        for l in xrange(num_labels):
            print 'mean label :  {}/{}'.format((l+1),num_labels)

            # set start/end index
            start_idx  = l
            end_idx    = l+1

            image_idx_list = numpy.where(train_label==l)[0]

            mean_feature = 0.
            count = 0.
            for i in image_idx_list:
                # read current image, get label for it
                image_data = cv2.imread(image_prefix+train_name[i])

                # crop image
                image_data = crop_square_image(image_data, 224)

                # transpose
                image_data = image_data.transpose(2,0,1)/255.
                image_data = image_data.reshape((1,)+image_data.shape)
                # feature extraction/ save labels
                image_feature = feature_extractor(image_data.astype(theano.config.floatX))
                mean_feature += image_feature
                count += 1.
            vector_feature[start_idx:end_idx] = mean_feature/count
            target_feature[start_idx:end_idx] = l



        # label each dataset axis
        vector_feature.dims[0].label = 'batch'
        vector_feature.dims[1].label = 'feature'
        target_feature.dims[0].label = 'batch'
        target_feature.dims[1].label = 'label'

        split_dict = {'train': {'vector_feature': ( 0,  num_labels),
                                'target_feature': ( 0,  num_labels)}}
        h5file.attrs['split'] = H5PYDataset.create_split_array(split_dict)

        h5file.flush()
        h5file.close()


if __name__=='__main__':
    data_split_file = 'whales_train_val_test_split.h5'
    feature_layer_name = 'pool4/3x3_s2'
    hdf5_file = '/data/lisatmp4/taesup/whales_feature_{}_'.format(feature_layer_name.replace('/','_')) + '_single_whale.h5'
    # build_single_whale_fuel_hdf5(feature_layer_name, data_split_file, hdf5_file, 5)
    build_pair_whale_fuel_hdf5(feature_layer_name=feature_layer_name,
                               data_split_file=data_split_file,
                               train_pair_hdf5_file='/data/lisatmp4/taesup/new_pair_train_data.h5',
                               train_hdf5_file='/data/lisatmp4/taesup/new_single_train_data.h5',
                               valid_hdf5_file='/data/lisatmp4/taesup/new_single_valid_data.h5',
                               test_hdf5_file='/data/lisatmp4/taesup/new_single_test_data.h5')

    # build_mean_whale_fuel_hdf5(feature_layer_name, data_split_file, '/data/lisatmp4/taesup/mean_train_data.h5')