import theano
import numpy
import pickle
from collections import OrderedDict
from utils.utils import merge_dicts
from utils.display import dispims
from layer.activations import Relu, Logistic, Tanh, LeakyRelu
from layer.layers import UniformRandomLayer, ConvolutionalLayer, LinearBatchNormalization, MaxPoolLayer, FlatteningLayer, LinearLayer, DropoutLayer, NormalRandomLayer, ConvBatchNormalization, ReshapingLayer
from layer.layer_utils import get_model_updates, get_tensor_output, get_L2_weight_decay
from optimizer.adagrad import AdaGrad
from optimizer.sgd import SGD
from utils.utils import normalize_data, unnormalize_data
from theano import tensor
from cost.binary_crossentropy import binary_crossentropy
from fuel.datasets.hdf5 import H5PYDataset
from fuel.streams import DataStream
from fuel.schemes import ShuffledScheme
from fuel.transformers import Flatten
floatX = theano.config.floatX


input_dim   = 1024
hidden_dim0 = 128

def set_metric_learner_model(batch_size=None):
    layers = []

    layers.append(ConvolutionalLayer(input_size=(7, 7),
                                     num_input_channels=832,
                                     filter_size=(5, 5),
                                     num_filters=1024,
                                     batch_size=None,
                                     name='conv0_layer'))
    layers.append(MaxPoolLayer(input_size=(3, 3),
                               num_input_channels=1024,
                               pool_size=(3, 3),
                               name='max0_layer'))
    layers.append(ConvBatchNormalization(input_size=(1,1),
                                         num_input_channels=1024,
                                         name='conv0_bn_layer'))
    layers.append(Relu(name='conv0_relu_layer'))


    layers.append(FlatteningLayer(name='flat_layer'))

    # layer 0
    layers.append(LinearLayer(input_dim=input_dim,
                              output_dim=hidden_dim0,
                              name='linear_layer0'))
    layers.append(LinearBatchNormalization(input_dim=hidden_dim0,
                                           name='bn_layer0'))
    layers.append(Relu(name='relu_layer0'))

    # layer output
    layers.append(LinearLayer(input_dim=hidden_dim0,
                              output_dim=1,
                              name='linear_output'))

    layers.append(Logistic(name='output'))
    return layers

# set full gan model
def set_siamese_model(batch_size, optimizer):

    # set metric learner layers
    metric_learner_layers     = set_metric_learner_model()

    # input data and label data
    pair_input_data  = tensor.tensor4('pair_input_data', dtype=floatX)
    pair_label_data  = tensor.matrix('pair_label_data' , dtype=floatX)

    # get pair distance of pair input data
    distance_data = get_tensor_output(pair_input_data, metric_learner_layers, is_training=True)

    # set cost for updating metric learner
    metric_learner_error_cost  = binary_crossentropy(distance_data, pair_label_data).mean()
    metric_learner_L2_cost     = 0.00001*get_L2_weight_decay(metric_learner_layers)

    metric_learner_cost = metric_learner_error_cost + metric_learner_L2_cost

    metric_learner_updates_dict = get_model_updates(layers=metric_learner_layers, cost=metric_learner_cost, optimizer=optimizer)

    # update function
    update_function = theano.function(inputs=[pair_input_data, pair_label_data],
                                      outputs=[metric_learner_cost,
                                               metric_learner_error_cost],
                                      updates=metric_learner_updates_dict,
                                      on_unused_input='ignore')


    # test pair input
    pair_input_data  = tensor.tensor4('pair_input_data', dtype=floatX)
    # get pair distance of pair input data
    distance_data = get_tensor_output(pair_input_data, metric_learner_layers, is_training=False)


    test_function = theano.function(inputs=[pair_input_data],
                                    outputs=distance_data)

    return update_function, test_function

def train_model(batch_size=100, num_epochs=100):
    optimizer = AdaGrad(learning_rate=0.01).update_params
    train_pair = H5PYDataset('/data/lisatmp4/taesup/new_pair_train_data.h5', which_sets=('train',))
    train_feat = H5PYDataset('/data/lisatmp4/taesup/new_single_train_data.h5', which_sets=('train',))
    valid_feat = H5PYDataset('/data/lisatmp4/taesup/new_single_valid_data.h5', which_sets=('valid',))

    train_pair_stream = DataStream.default_stream(dataset=train_pair,
                                                  iteration_scheme=ShuffledScheme(batch_size=batch_size,
                                                                                  examples=train_pair.num_examples))
    print 'num train_pairs : ', train_pair.num_examples

    train_feat_stream = DataStream.default_stream(dataset=train_feat,
                                                  iteration_scheme=ShuffledScheme(batch_size=train_feat.num_examples,
                                                                                  examples=train_feat.num_examples))

    valid_feat_stream = DataStream.default_stream(dataset=valid_feat,
                                                  iteration_scheme=ShuffledScheme(batch_size=1,
                                                                                  examples=valid_feat.num_examples))

    print 'num valids : ', valid_feat.num_examples
    whale_label_dict = pickle.load( open( "/data/lisatmp4/taesup/whale_label_dict.pkl", "rb" ) )

    update_function, test_function = set_siamese_model(batch_size, optimizer)
    valid_recent_cost = 0.

    for e in xrange(num_epochs):
        print '================================================================'
        print 'EPOCH #{}'.format(e)
        print '================================================================'

        # train phase
        train_pair_batch_iters = train_pair_stream.get_epoch_iterator()
        for b, train_batch_pair_data in enumerate(train_pair_batch_iters):
            outputs = update_function(numpy.abs(train_batch_pair_data[1]), train_batch_pair_data[0])
            if (b)%5==0:
                print 'EPOCH #{}'.format(e),' : batch #{}'.format(b), 'SIAMESE WHALE'
                print '================================================================'
                print ' normal      cost   : ', outputs[1]
                print '----------------------------------------------------------------'
                print ' regularized cost   : ', outputs[0]
                print '----------------------------------------------------------------'
                print ' current valid cost : ', valid_recent_cost
                print '================================================================'
        if e%10==0:
            valid_raw_data_iters = valid_feat_stream.get_epoch_iterator()
            valid_cost  = 0.
            valid_count = 0.

            for valid_data in valid_raw_data_iters:
                valid_input_data = valid_data[1]
                valid_label_data = valid_data[0][0][0]

                train_raw_data_iters = train_feat_stream.get_epoch_iterator()
                train_batch_data = train_raw_data_iters.next()
                train_input_data = train_batch_data[1]

                pair_input_data = numpy.abs(train_input_data-valid_input_data)
                distance_output = -test_function(pair_input_data)
                distance_output = distance_output - distance_output.max(axis=0)
                distance_output = numpy.exp(distance_output)
                distance_output = distance_output/distance_output.sum(axis=0)
                label_prob      = distance_output
                valid_cost     += -numpy.log(label_prob[valid_label_data][0])
                valid_count    += 1.

            valid_recent_cost = float(valid_cost)/float(valid_count)

        # if (e+1)%50==0:
        #
        #     # test
        #     test_feat  = H5PYDataset('/data/lisatmp4/taesup/single_test_data.h5', which_sets=('test',))
        #     test_feat_stream = DataStream.default_stream(dataset=test_feat,
        #                                                  iteration_scheme=ShuffledScheme(batch_size=1,
        #                                                                                  examples=test_feat.num_examples))
        #
        #
        #     test_output_dict = OrderedDict()
        #     test_raw_data_iters = test_feat_stream.get_epoch_iterator()
        #     for test_data in test_raw_data_iters:
        #         test_input_data = test_data[1]
        #         test_name       = test_data[0][0][0]
        #         min_distance  = 10.
        #         min_label_idx = -1
        #         train_raw_data_iters = train_feat_stream.get_epoch_iterator()
        #         for train_batch_data in train_raw_data_iters:
        #             train_input_data = train_batch_data[1]
        #             train_label_data = train_batch_data[0]
        #
        #             pair_input_data = numpy.abs(train_input_data-test_input_data)
        #             distance_output = test_function(pair_input_data)
        #
        #             min_idx = numpy.argmin(distance_output, axis=0)
        #             if distance_output[min_idx]<min_distance:
        #                 min_distance  = distance_output[min_idx]
        #                 min_label_idx = train_label_data[min_idx][0][0]
        #
        #         test_output_dict[test_name.split('.')[0]] = whale_label_dict.keys()[whale_label_dict.values().index(min_label_idx)]
        #
        #     pickle.dump(test_output_dict, open( "/data/lisatmp4/taesup/whale_test_prediction_{}.pkl".format(e+1), "wb" ) )



if __name__=="__main__":

    train_model(batch_size=100, num_epochs=500)