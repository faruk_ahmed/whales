from collections import OrderedDict
from itertools import izip

import numpy as np
import theano
from theano import tensor as T

class Optimizer(object):
    def update_params(self, param, grad):
        raise NotImplementedError('not implemented')

