__author__ = 'KimTS'
import theano
import numpy as np
from collections import OrderedDict
from optimizer import Optimizer

class SGD(Optimizer):
    """
    ==================================================================
    Stochastic Gradient Descent (SGD) with Momentum
    ==================================================================
    velocity (t+1) = momentum*velocity(t) - learning_rate * gradient(t+1)
    parameter(t+1) = parameter(t) + velocity(t+1)
    """
    def __init__(self,
                 learning_rate=0.01,
                 momentum=0.5):
        self.learning_rate = learning_rate
        self.momentum = momentum

    def update_params(self, param, grad):
        updates = OrderedDict()
        value  = param.get_value(borrow=True)
        velocity = theano.shared(np.zeros(value.shape, dtype=value.dtype), broadcastable=param.broadcastable)
        velocity_new = self.momentum*velocity - self.learning_rate*grad
        updates[velocity] = velocity_new
        updates[param] = param + velocity_new
        return updates

    # def update_params(self, param, grad):
    #     updates = OrderedDict()
    #     updates[param] = param - self.learning_rate*grad
    #     return updates