from collections import OrderedDict
from optimizer import Optimizer
import theano
from theano import tensor
import numpy as np

class AdaDelta(Optimizer):
    def __init__(self,
                 learning_rate=1.0,
                 rho=0.95,
                 eps=1e-6):
        self.learning_rate = learning_rate
        self.rho = rho
        self.eps = eps


    def update_params(self, param, grad):
        updates = OrderedDict()

        value = param.get_value(borrow=True)

        accumulate       = theano.shared(np.zeros(value.shape, dtype=value.dtype), broadcastable=param.broadcastable)
        delta_accumulate = theano.shared(np.zeros(value.shape, dtype=value.dtype), broadcastable=param.broadcastable)

        accumulate_update = self.rho * accumulate + (1 - self.rho) * grad ** 2
        updates[accumulate] = accumulate_update

        update = (grad * tensor.sqrt(delta_accumulate + self.eps)/tensor.sqrt(accumulate_update + self.eps))
        updates[param] = param - self.learning_rate * update

        delta_accumulate_update = self.rho * delta_accumulate + (1 - self.rho) * update ** 2
        updates[delta_accumulate] = delta_accumulate_update

        return updates
