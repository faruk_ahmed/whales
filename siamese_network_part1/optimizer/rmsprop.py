__author__ = 'KimTS'
from collections import OrderedDict
from optimizer import Optimizer
import theano
import numpy as np
from theano import tensor as T
class RmsProp(Optimizer):
    def __init__(self,
                 learning_rate=1.0,
                 momentum=0.9,
                 eps=1e-20):
        self.learning_rate = learning_rate
        self.rho = momentum
        self.eps = eps


    def update_params(self, param, grad):
        updates = OrderedDict()

        value = param.get_value(borrow=True)
        accu = theano.shared(np.zeros(value.shape, dtype=value.dtype), broadcastable=param.broadcastable)
        accu_new = self.rho * accu + (1 - self.rho) * grad ** 2
        updates[accu] = accu_new
        updates[param] = param - (self.learning_rate * grad / T.sqrt(accu_new + self.eps))
        return updates