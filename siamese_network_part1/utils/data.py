__author__ = 'KimTS'
import numpy as np
from utils import get_dataset_types, get_dataset_size, get_batch_data

class DataIter(object):
    def __init__(self,
                 data_path,
                 shuffle=False,
                 batch_size=100):
        self.data_path  = data_path
        self.batch_size = batch_size
        self.data_types = get_dataset_types(self.data_path)
        self.data_shape = get_dataset_size(self.data_path)
        self.num_data   = self.data_shape[0]
        self.data_shape = self.data_shape[1:]

        self.num_batches = np.ceil(float(self.num_data)/float(self.num_batches))
        self.curr_batch  = -1

        self.batch_idx = np.arange(self.num_data)

    def reset(self):
        self.curr_batch = 0
        np.random.shuffle(self.batch_idx)

    def __iter__(self):
        return self

    def __next__(self):
        self.curr_batch += 1
        curr_batch_size = self.batch_size
        if self.curr_batch==self.num_batches-1:
            curr_batch_size = self.num_data-self.curr_batch*self.batch_size
        elif self.curr_batch>=self.num_batches:
            self.reset()
            return StopIteration()

        data = []
        curr_batch_idx = self.batch_idx[self.curr_batch*self.batch_size:self.curr_batch*self.batch_size+curr_batch_size]
        for type in self.data_types:
            data.append(get_batch_data(self.data_path, type, curr_batch_idx))








