import numpy
import matplotlib
matplotlib.use('Agg')
import pylab

def dispims(M, height, width, border=0, bordercolor=0.0, savePath=None, layout=None, **kwargs):
    """ Display a stack of vectorized matrices (colunmwise). 
    """
    numimages = M.shape[1]
    if layout is None:
        n0 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
        n1 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
    else:
        n0, n1 = layout
    im = bordercolor * numpy.ones(((height+border)*n0+border,(width+border)*n1+border),dtype='<f8')
    for i in range(n0):
        for j in range(n1):
            if i*n1+j < M.shape[1]:
                im[i*(height+border)+border:(i+1)*(height+border)+border,
                   j*(width+border)+border :(j+1)*(width+border)+border] = numpy.vstack((
                            numpy.hstack((numpy.reshape(M[:,i*n1+j],(height, width)),
                                   bordercolor*numpy.ones((height,border),dtype=float))),
                            bordercolor*numpy.ones((border,width+border),dtype=float)
                            ))
    pylab.imshow(im, cmap=pylab.cm.gray, interpolation='nearest', **kwargs)
    if savePath is not None:
        pylab.savefig(savePath)
    pylab.close()

def display_pair_images(M0, M1, height, width, border=2, bordercolor=1.0, savePath=None, layout=None, **kwargs):
    """ Display a stack of vectorized matrices (colunmwise).
    """
    numimages = M0.shape[1]
    if M1.shape[1]!=numimages:
        ValueError('different sized input')
    if layout is None:
        n0 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
        n1 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
    else:
        n0, n1 = layout
    im = bordercolor * numpy.ones(((height+border)*n0+border,(width*2+border)*n1+border),dtype='<f8')
    for i in range(n0):
        for j in range(n1):
            if i*n1+j < M0.shape[1]:
                im[i*(height+border)+border:(i+1)*(height+border)+border,
                   j*(width*2+border)+border :(j+1)*(width*2+border)+border] = numpy.vstack((numpy.hstack((numpy.reshape(M0[:,i*n1+j],(height, width)), numpy.reshape(M1[:,i*n1+j],(height, width)), bordercolor*numpy.ones((height,border),dtype=float))), bordercolor*numpy.ones((border,width*2+border),dtype=float)))
    pylab.imshow(im, cmap=pylab.cm.gray, interpolation='nearest', **kwargs)
    if savePath is not None:
        pylab.savefig(savePath)
    pylab.close()
    
def dispims_color(M, border=0, bordercolor=[0.0, 0.0, 0.0], savePath=None, *imshow_args, **imshow_keyargs):
    """ Display an array of rgb images. 
    The input array is assumed to have the shape numimages x numpixelsY x numpixelsX x 3
    """
    bordercolor = numpy.array(bordercolor)[None, None, :]
    numimages = len(M)
    M = M.copy()
    for i in range(M.shape[0]):
        M[i] -= M[i].flatten().min()
        M[i] /= M[i].flatten().max()
    height, width, three = M[0].shape
    assert three == 3
    
    n0 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
    n1 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
    im = numpy.array(bordercolor)*numpy.ones(
                             ((height+border)*n1+border,(width+border)*n0+border, 1),dtype='<f8')
    for i in range(n0):
        for j in range(n1):
            if i*n1+j < numimages:
                im[j*(height+border)+border:(j+1)*(height+border)+border,
                   i*(width+border)+border:(i+1)*(width+border)+border,:] = numpy.concatenate((
                  numpy.concatenate((M[i*n1+j,:,:,:],
                         bordercolor*numpy.ones((height,border,3),dtype=float)), 1),
                  bordercolor*numpy.ones((border,width+border,3),dtype=float)
                  ), 0)
    imshow_keyargs["interpolation"]="nearest"
    pylab.imshow(im, *imshow_args, **imshow_keyargs)
    
    if savePath == None:
        pylab.show()
    else:
        pylab.savefig(savePath)
    pylab.close()

def display_pair_color_images(M0, M1,height, width,border=10, bordercolor=[0.0, 0.0, 0.0], savePath=None, *imshow_args, **imshow_keyargs):
    bordercolor = numpy.array(bordercolor)[None, None, :]
    if M0.shape!=M1.shape:
        ValueError('different sized input')
    M0 = M0.T
    M1 = M1.T
    numimages = M0.shape[0]
    M0 = M0.reshape(numimages, 3, height, width)
    M1 = M1.reshape(numimages, 3, height, width)
    M0 = M0.swapaxes(1,2)
    M0 = M0.swapaxes(2,3)
    M1 = M1.swapaxes(1,2)
    M1 = M1.swapaxes(2,3)

    M0 = M0.copy()
    M1 = M1.copy()
    for i in range(M0.shape[0]):
        M0[i] -= M0[i].flatten().min()
        M0[i] /= M0[i].flatten().max()
        M1[i] -= M1[i].flatten().min()
        M1[i] /= M1[i].flatten().max()
    height, width, three = M0[0].shape
    assert three == 3

    n0 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
    n1 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
    im = numpy.array(bordercolor)*numpy.ones(((height+border)*n1+border,(width*2+border)*n0+border, 1),dtype='<f8')

    for i in range(n0):
        for j in range(n1):
            if i*n1+j < numimages:
                im[j*(height+border)+border:(j+1)*(height+border)+border,
                   i*(width*2+border)+border:(i+1)*(width*2+border)+border,:] = numpy.concatenate((numpy.concatenate((M0[i*n1+j,:,:,:], M1[i*n1+j,:,:,:], bordercolor*numpy.ones((height,border,3),dtype=float)), 1), bordercolor*numpy.ones((border,width*2+border,3),dtype=float)), 0)
    pylab.imshow(im, *imshow_args, **imshow_keyargs)
    if savePath is not None:
        pylab.savefig(savePath)
    pylab.close()
def display_color_images(M0,height, width,border=10, bordercolor=[0.0, 0.0, 0.0], savePath=None, *imshow_args, **imshow_keyargs):
    bordercolor = numpy.array(bordercolor)[None, None, :]
    numimages = M0.shape[0]
    M0 = M0.reshape(numimages, 3, height, width)
    M0 = M0.swapaxes(1,2)
    M0 = M0.swapaxes(2,3)

    M0 = M0.copy()
    for i in range(M0.shape[0]):
        M0[i] -= M0[i].flatten().min()
        M0[i] /= M0[i].flatten().max()
    height, width, three = M0[0].shape
    assert three == 3

    n0 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
    n1 = numpy.int(numpy.ceil(numpy.sqrt(numimages)))
    im = numpy.array(bordercolor)*numpy.ones(((height+border)*n1+border,(width+border)*n0+border, 1),dtype='<f8')

    for i in range(n0):
        for j in range(n1):
            if i*n1+j < numimages:
                im[j*(height+border)+border:(j+1)*(height+border)+border,
                   i*(width+border)+border:(i+1)*(width+border)+border,:] = numpy.concatenate((
                  numpy.concatenate((M0[i*n1+j,:,:,:],
                         bordercolor*numpy.ones((height,border,3),dtype=float)), 1),
                  bordercolor*numpy.ones((border,width+border,3),dtype=float)
                  ), 0)
    pylab.imshow(im, *imshow_args, **imshow_keyargs)
    if savePath is not None:
        pylab.savefig(savePath)
    pylab.close()
import matplotlib.pyplot as plt
def plot_learning_curve(cost_values, cost_names, save_as):
    for cost in cost_values:
        plt.plot(xrange(len(cost)), cost)

    plt.legend(cost_names, loc='upper right')
    plt.savefig(save_as)
    plt.close()
