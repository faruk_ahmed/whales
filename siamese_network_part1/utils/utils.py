__author__ = 'KimTS'
import cPickle
import os
import shutil
import sys
import tempfile
import numpy
import operator
import h5py
from collections import OrderedDict
def prefix_name(prefix, name):
    return '%s_%s' % (prefix, name)

def normalize_data(data, mean, std):
    return  (data-mean)/(std+1e-20)

def unnormalize_data(data, mean, std):
    return  data*(std+1e-20)+mean

def merge_dicts(list_of_dicts):
    merged_dict = OrderedDict()

    for dict in list_of_dicts:
        for key, value in dict.iteritems():
            if key in merged_dict:
                Warning('duplicate things')
            merged_dict[key] = value

    return merged_dict


def get_tensor_params_val(tensor_params_dict):
    params_dict = OrderedDict()
    for param_name, param_tensor in tensor_params_dict.iteritems():
        params_dict[param_name] = param_tensor.get_value()
    return params_dict


def save_params_val(params_dict, save_to):
    numpy.savez(save_to, **params_dict)

def secure_pickle_dump(object_, path):
    """
    This code is brought from Blocks.
    Robust serialization - does not corrupt your files when failed.
    Parameters
    ----------
    object_ : object
        The object to be saved to the disk.
    path : str
        The destination path.
    """
    try:
        d = os.path.dirname(path)
        with tempfile.NamedTemporaryFile(delete=False, dir=d) as temp:
            try:
                cPickle.dump(object_, temp, protocol=-1)
            except RuntimeError as e:
                if str(e).find('recursion') != -1:
                    Warning('cle.utils.secure_pickle_dump encountered '
                        'the following error: ' + str(e) +
                        '\nAttempting to resolve this error by calling ' +
                        'sys.setrecusionlimit and retrying')
                    old_limit = sys.getrecursionlimit()
                try:
                    sys.setrecursionlimit(50000)
                    cPickle.dump(object_, temp, protocol=-1)
                finally:
                    sys.setrecursionlimit(old_limit)
        shutil.move(temp.name, path)
    except:
        if "temp" in locals():
            os.remove(temp.name)
        raise

def unpickle(path):
    f = open(path, 'rb')
    object_ = cPickle.load(f)
    f.close()
    return object_


def get_word_dict(document_path):
    with open(name=document_path, mode='r') as f:
        lines = f.readlines()

    word_hist = OrderedDict()
    for l, line in enumerate(lines):
        words = line.split()
        for w, word in enumerate(words):
            if word in word_hist:
                word_hist[word] += 1
            else:
                word_hist[word] = 1

    word_hist = sorted(word_hist.items(), key=operator.itemgetter(1), reverse=True)
    word_dict = OrderedDict()

    word_idx = 0
    for word in word_hist:

        word_dict[word] = word_idx
        word_idx += 1

    return word_dict

def get_dataset_types(data_path):
    with h5py.File(data_path,'r') as file:
        return  file.keys()

def get_dataset_size(data_path, data_type=None):
    with h5py.File(data_path,'r') as file:
        if data_type is None:
            data_type = file.keys()[0]
        return  file[data_type].shape

def get_batch_data(data_path, data_type, idx):
    with h5py.File(data_path,'r') as file:
        if data_type is None:
            data_type = file.keys()[0]
        return file[data_type][numpy.sort(idx).tolist()]