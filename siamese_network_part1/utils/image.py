import numpy as np
from numpy.random import choice,rand,randint
from skimage import img_as_float, img_as_ubyte
from skimage.transform import warp, AffineTransform, resize
from skimage.util import pad

# crop square image
def crop_square_image(input_image, target_size):
    # get image size
    height, width = input_image.shape[0], input_image.shape[1]

    # crop image from center
    output_image = input_image.copy()
    if height>width:
        idx0 = int(height/2.)-int(width/2.)
        idx1 = idx0+width
        output_image = output_image[idx0:idx1]
    elif width<height:
        idx0 = int(width/2.)-int(height/2.)
        idx1 = idx0+height
        output_image = output_image[:,idx0:idx1]

    # resize cropped image
    output_image = resize(output_image, (target_size, target_size))
    return output_image

# image transformation
def image_transform(input_image,
                    input_size,
                    num_transforms=1,
                    scale_min=1.,
                    scale_max=1.,
                    rotation_max_degree=0,
                    translation_minmax_rel_to_imsize=(0, 0),
                    rand_flip=(True, True),
                    pad_max=0):
    # copy input image
    reshaped = input_image.copy()

    # get input data type
    if input_image.dtype == np.uint8:
        reshaped = img_as_float(reshaped)
    elif not np.issubdtype(input_image.dtype, np.float):
        raise ValueError('expected either float or int image')

    origin_to_center     = AffineTransform(translation=-np.array(reshaped.shape[:2]) / 2)
    origin_to_orig_coord = AffineTransform(translation=np.array(reshaped.shape[:2]) / 2)

    output_images = np.zeros((num_transforms, input_size, input_size, input_image.shape[-1]))
    for i in xrange(num_transforms):
        translation_vector = rand(1, 2) * (translation_minmax_rel_to_imsize * np.array(input_image.shape[:2]))[None, :]
        rotation_angle     = np.radians(rand(1, 1)*2.0*rotation_max_degree - rotation_max_degree)
        scale_factor       = rand(1, 1) * (scale_max - scale_min) + scale_min
        ver_flip           = np.where(rand_flip[0], choice((-1,1), size=1), np.ones(1, dtype=int))
        hor_flip           = np.where(rand_flip[1], choice((-1,1), size=1), np.ones(1, dtype=int))

        transformed_image  = AffineTransform(translation=translation_vector,
                                              rotation=rotation_angle,
                                              scale=(scale_factor, scale_factor))
        transformed_image += origin_to_center + origin_to_orig_coord

        transformed_image = warp(reshaped, transformed_image, mode='constant', cval=-1.)[::ver_flip,::hor_flip]
        img_area  = np.where(transformed_image!=-1)
        if len(img_area[0])<=0:
            output_images[i] = crop_square_image(reshaped,input_size)
        else:
            min_r, max_r = min(img_area[0]), max(img_area[0])
            min_c, max_c = min(img_area[1]), max(img_area[1])

            transformed_image = transformed_image[min_r:max_r+1, min_c:max_c+1,:]
            transformed_image = np.clip(transformed_image,0., 255)

            height, width = transformed_image.shape[0], transformed_image.shape[1]

            if height<input_size or width<input_size:
                if height<width:
                    transformed_image = resize(transformed_image, (input_size, input_size*float(width/height)))
                else:
                    transformed_image = resize(transformed_image, (input_size*float(height/width), input_size))

            height, width = transformed_image.shape[0], transformed_image.shape[1]

            idx_r = np.random.randint(0, height-input_size+1,1)
            idx_c = np.random.randint(0, width-input_size+1,1)
            transformed_image = transformed_image[idx_r:(idx_r+input_size),idx_c:(idx_c+input_size)]
            output_images[i] = transformed_image
    return output_images