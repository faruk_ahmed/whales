#!/usr/bin/env python
#-*- coding: utf-8 -*-


from glob import glob
import numpy as np
from PIL import Image
import os

from common.imagetools import rescale_shorter_side

tar_res = 256

#src_folder = '/data/lisatmp3/michals/data/whales/imgs_crop'
#tar_folder = '/data/lisatmp3/michals/data/whales/imgs_crop_resized'
src_folder = '/storeSSD/data/whales/data/imgs_crop'
tar_folder = '/storeSSD/data/whales/data/imgs_crop_resized'

# get train_im paths
print os.path.join(src_folder, 'whale_*', '*.jpg')
files = glob(os.path.join(src_folder, 'whale_*', '*.jpg'))
train_tar_subfolders = [os.path.split(f)[0].replace(
    src_folder, tar_folder) for f in files]

# create subfolders
for folder in train_tar_subfolders:
    os.system('mkdir -p {0}'.format(folder))

# get test_im paths
files.extend(glob(os.path.join(src_folder, '*.jpg')))

target_fnames = [f.replace(src_folder, tar_folder) for f in files]

# resize images
for w, (wfile, tar_fname) in enumerate(zip(files, target_fnames)):
    if (w + 1) % 100 == 0:
        print '{0}/{1}'.format(w+1,len(files))
    im = np.array(Image.open(wfile))
    im = Image.fromarray(rescale_shorter_side(im, 256))
    im.save(tar_fname)


#if __name__ == '__main__':
#    import matplotlib.pyplot as plt
#    from scipy.misc import face
#
#
#    im = face()
#    im2 = rescale_shorter_side(im, 256)
#    im3 = rescale_shorter_side(im.transpose(1,0,2), 256)
#    plt.subplot(131)
#    plt.imshow(im)
#    plt.subplot(132)
#    plt.imshow(im2)
#    plt.subplot(133)
#    plt.imshow(im3)
#    plt.savefig('test.png')




# vim: set ts=4 sw=4 sts=4 expandtab:
