#!/usr/bin/env python
#-*- coding: utf-8 -*-


import sys
sys.path.append('..')
from warnings import warn

import numpy as np
import tables

from common.misc import onehot

numpy_rng = np.random.RandomState(1)

warn('THIS SCRIPT REQUIRES SOME PRIOR STEPS! IF YOU JUST WANT THE SPLIT,'
        ' SEE whales_train_val_test_split.h5')

val_ratio = .2
feature_file = '/cuda4/storessd/data/whales/whales_crop_resize_pad_rgb_224_features_pool4--3x3_s2.h5'
feature_file_train_val_split = 'whales_crop_resize_pad_rgb_224_features_pool4--3x3_s2_train_val_test.h5'

with tables.openFile(feature_file) as h5file:
    features = h5file.root.train_features.read()
    labels = h5file.root.train_labels.read()
    fnames = h5file.root.train_fnames.read()
    classes = h5file.root.classes.read()
    test_features = h5file.root.test_features.read()
    test_fnames = h5file.root.test_fnames.read()

nsamples = len(features)

nval = int(nsamples * val_ratio)

class_counts = onehot(labels, numclasses=max(labels)+1).sum(0)
print class_counts

val_indices = []
for i in range(nval):
    class_idx = class_counts.argmax()
    rows = np.where(labels == class_idx)[0]
    print rows
    # filter out rows that are already in val set
    rows = filter(lambda x: x not in val_indices, rows)
    print rows
    selected_row = rows[numpy_rng.randint(len(rows))]
    val_indices.append(selected_row)
    class_counts[class_idx] -= 1

train_indices = set(range(len(features)))
train_indices -= set(val_indices)

train_indices = np.array(list(train_indices))
val_indices = np.array(val_indices)

print 'n train:', len(train_indices)
print 'n_val:', len(val_indices)
print 'n_test:', len(test_features)

with tables.openFile(feature_file_train_val_split, 'w') as h5file:
    h5file.createArray(h5file.root, 'train_features', features[train_indices])
    h5file.createArray(h5file.root, 'train_labels', labels[train_indices])
    h5file.createArray(h5file.root, 'train_fnames', fnames[train_indices])
    h5file.createArray(h5file.root, 'val_features', features[val_indices])
    h5file.createArray(h5file.root, 'val_labels', labels[val_indices])
    h5file.createArray(h5file.root, 'val_fnames', fnames[val_indices])
    h5file.createArray(h5file.root, 'test_features', test_features)
    h5file.createArray(h5file.root, 'test_fnames', test_fnames)
    h5file.createArray(h5file.root, 'classes', classes)




# vim: set ts=4 sw=4 sts=4 expandtab:
