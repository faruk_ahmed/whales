#!/usr/bin/env python
#-*- coding: utf-8 -*-

import cPickle as pickle
import sys
sys.path.insert(0, '..')

import numpy as np
import tables

#from common import pca
from sklearn.decomposition import PCA

SKIP_COMPUTE_TRANSFORM = False

feature_layer_name = 'pool4/3x3_s2'
imsize = 224
feature_file = ('/cuda4/storessd/data/whales/whales_crop_resize'
                '_pad_{0}_features_{1}.h5').format(
                    imsize,
                    feature_layer_name.replace('/', '--'))

pca_feature_file = ('whales_crop_resize'
                '_pad_{0}_features_{1}_pca.h5').format(
                    imsize,
                    feature_layer_name.replace('/', '--'))

#feature_file = ('/storeSSD/data/whales/whales_crop_resize'
#                '_pad_{0}_features_{1}.h5').format(
#                    imsize,
#                    feature_layer_name.replace('/', '--'))
#
#pca_feature_file = ('/storeSSD/data/whales/whales_crop_resize'
#                '_pad_{0}_features_{1}_pca.h5').format(
#                    imsize,
#                    feature_layer_name.replace('/', '--'))

with tables.openFile(feature_file) as h5file:
    train_features = h5file.root.train_features.read()
    test_features = h5file.root.test_features.read()
    train_labels = h5file.root.train_labels.read()
    classes = h5file.root.classes.read()
    train_fnames = h5file.root.train_fnames.read()
    test_fnames = h5file.root.test_fnames.read()

assert abs(train_features).mean() > 1e-7

if not SKIP_COMPUTE_TRANSFORM:
    print 'computing PCA transform'
    #pca_transf, inv_pca_transf, mean0, std0, var_fracs = pca.pca(
    #    train_features.reshape(train_features.shape[0], -1),
    #    use_gpu=False, batchsize=100)
    pca = PCA(n_components=.98, whiten=True)
    pca.fit(train_features.reshape(train_features.shape[0], -1))
    print 'pca_var_frac: ', np.cumsum(pca.explained_variance_ratio_)

    pickle.dump(pca.get_params(), file('pca_params.pkl', 'w'))

    #with tables.openFile('pca_params.h5', 'w') as pca_file:
    #    pca_file.createArray(pca_file.root, 'pca_transf', pca_transf)
    #    pca_file.createArray(pca_file.root, 'inv_pca_transf', inv_pca_transf)
    #    pca_file.createArray(pca_file.root, 'mean0', mean0)
    #    pca_file.createArray(pca_file.root, 'std0', std0)
    #    pca_file.createArray(pca_file.root, 'var_fracs', var_fracs)
else:
    print 'loading pca params'
    #with tables.openFile('pca_params.h5') as pca_file:
    #    pca_transf = pca_file.root.pca_transf.read()
    #    inv_pca_transf = pca_file.root.inv_pca_transf.read()
    #    mean0 = pca_file.root.mean0.read()
    #    std0 = pca_file.root.std0.read()
    #    var_fracs = pca_file.root.var_fracs.read()
    pca.set_params(pickle.load(file('pca_params.pkl')))
print 'done'


print 'projecting train features...'
train_features = pca.transform(
    train_features.reshape(train_features.shape[0], -1))
#train_features = pca.whiten(
#    train_features.reshape(train_features.shape[0], -1),
#    V=pca_transf,
#    m0=mean0, s0=std0, var_fracs=var_fracs,
#    retain_var=.999, use_gpu=False)
print 'done'

print 'projecting test features...'
test_features = pca.transform(
    test_features.reshape(test_features.shape[0], -1))
#test_features = pca.whiten(
#    test_features.reshape(test_features.shape[0], -1),
#    V=pca_transf,
#    m0=mean0, s0=std0, var_fracs=var_fracs,
#    retain_var=.999, use_gpu=False)
print 'done'

with tables.openFile(pca_feature_file, 'w') as h5file:
    h5file.createArray(h5file.root, 'train_features', train_features)
    h5file.createArray(h5file.root, 'test_features', test_features)
    h5file.createArray(h5file.root, 'train_labels', train_labels)
    h5file.createArray(h5file.root, 'classes', classes)
    h5file.createArray(h5file.root, 'train_fnames', train_fnames)
    h5file.createArray(h5file.root, 'test_fnames', test_fnames)


# vim: set ts=4 sw=4 sts=4 expandtab:
