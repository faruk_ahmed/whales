#!/usr/bin/env python
#-*- coding: utf-8 -*-

from glob import glob
import os
from PIL import Image
import sys
sys.path.append('..')

import matplotlib.pyplot as plt
import numpy as np
from skimage import img_as_float
from sklearn_theano.feature_extraction import caffe, overfeat
import tables
import theano
from theano.sandbox.rng_mrg import MRG_RandomStreams
import theano.tensor as T

from common.layers import RandCropAndFlip, RandRot90
from common.imagetools import rand_transforms, rescale_and_pad
from common.misc import onehot

numpy_rng = np.random.RandomState(1)
theano_rng = MRG_RandomStreams(1)

feature_layer_name = 'pool4/3x3_s2'
imsize = 224
batchsize = 256

datafile = ('/storeSSD/data/whales/whales_crop_resize_pad'
            '_{0}.h5').format(imsize)
feature_file = ('/storeSSD/data/whales/whales_crop_resize'
                '_pad_{0}_features_{1}.h5').format(
                    imsize,
                    feature_layer_name.replace('/', '--'))
image_dir = '/storeSSD/data/whales/data/imgs_crop'
train_files = glob(os.path.join(image_dir, 'whale_*', '*.jpg'))
test_files = glob(os.path.join(image_dir, '*.jpg'))

if not image_dir.endswith('/'):
    image_dir += '/'

test_im = np.array(Image.open(train_files[0]))

if test_im.ndim == 2:
    nchannels = 1
elif test_im.ndim == 3:
    if test_im.shape[2] == 3:
        nchannels = 3
    elif test_im.shape[2] == 1:
        nchannels = 1
    else:
        raise ValueError()
else:
    raise ValueError()

SKIP_RESCALE = True

if not SKIP_RESCALE:

    rel_paths = [w.replace(image_dir, '') for w in train_files]
    rel_test_paths = [w.replace(image_dir, '') for w in test_files]

    train_labels = np.array(
        [os.path.split(rpath)[0] for rpath in rel_paths])
    # collect unique whale ids and convert to 0-based class indices
    classes = sorted(np.unique(train_labels))
    train_labels = np.array(map(classes.index, train_labels))
    train_fnames = np.array(rel_paths)
    test_fnames = np.array(rel_test_paths)

    train_ims = np.zeros(
        (len(train_files), imsize, imsize, nchannels),
        dtype=test_im.dtype)

    test_ims = np.zeros(
        (len(test_files), imsize, imsize, nchannels),
        dtype=test_im.dtype)

    print 'resizing and padding train images...'
    # iterate over files (apply augmentation here?)
    for i, fname in enumerate(train_files):
        if (i + 1) % 100 == 0:
            print '{0}/{1}'.format(i+1, len(train_files))
        im = np.array(Image.open(fname))
        train_ims[i] = rescale_and_pad(im, imsize, pad_val=0).reshape(
            train_ims.shape[1:])
    print 'done'

    print 'resizing and padding test images...'
    # iterate over test files
    for i, fname in enumerate(test_files):
        if (i + 1) % 100 == 0:
            print '{0}/{1}'.format(i+1, len(test_files))
        im = np.array(Image.open(fname))
        test_ims[i] = rescale_and_pad(im, imsize, pad_val=0).reshape(
            test_ims.shape[1:])
    print 'done'

    print 'dumping to hdf5 file'
    with tables.openFile(datafile, 'w') as h5file:
        h5file.createArray(h5file.root, 'train_ims', train_ims)
        h5file.createArray(h5file.root, 'train_labels', train_labels)
        h5file.createArray(h5file.root, 'classes', classes)
        h5file.createArray(h5file.root, 'train_fnames', train_fnames)
        h5file.createArray(h5file.root, 'test_ims', test_ims)
        h5file.createArray(h5file.root, 'test_fnames', test_fnames)
else:
    print 'loading prescaled and padded images'
    with tables.openFile(datafile) as h5file:
        train_ims = h5file.root.train_ims.read()
        train_labels = h5file.root.train_labels.read()
        classes = h5file.root.classes.read()
        train_fnames = h5file.root.train_fnames.read()
        test_ims = h5file.root.test_ims.read()
        test_fnames = h5file.root.test_fnames.read()

print 'building theano graph of googlenet'
# build graph:
theanolayers, input_var, params = \
    caffe.googlenet.create_theano_expressions(
        theano_rng=theano_rng, return_params=True
    )
print 'done'

#for i, k in enumerate(theanolayers.keys()):
#    print '{0}) {1}'.format(i,k)
#print theanolayers.keys()


print 'compiling function for feature extraction...'
fn = theano.function(
    [input_var], theanolayers[feature_layer_name])
print 'done'

test_feat = fn(
    train_ims[0:1].transpose(0,3,1,2).repeat(
        3, axis=1).astype(np.float32))

with tables.openFile(feature_file, 'w') as h5file:
    # allocate arrays for train and test features
    train_features = h5file.createCArray(
        h5file.root,
        'train_features',
        tables.Float32Atom(),
        (len(train_ims), ) + test_feat.shape[1:])
    test_features = h5file.createCArray(
        h5file.root,
        'test_features',
        tables.Float32Atom(),
        (len(test_ims), ) + test_feat.shape[1:])
    # copy labels, class list and file names
    h5file.createArray(h5file.root, 'train_labels', train_labels)
    h5file.createArray(h5file.root, 'classes', classes)
    h5file.createArray(h5file.root, 'train_fnames', train_fnames)
    h5file.createArray(h5file.root, 'test_fnames', test_fnames)

    print 'extracting train features...'
    for i in range(len(train_ims)//batchsize):
        print '{0}/{1}'.format(i, len(train_ims)//batchsize)
        start = (i * batchsize)
        end = ((i + 1) * batchsize)
        print 'start,end: {0}, {1}'.format(start,end, )

        batch = train_ims[start:end].transpose(0,3,1,2)
        if nchannels == 1:
            batch = batch.repeat(3, axis=1)
        print 'shape of batch: {0}'.format(batch.shape, )
        print 'shape of train_features[start:end]: {0}'.format(train_features[start:end].shape, )
        train_features[start:end] = fn(batch.astype(np.float32))

    undershoot = len(train_ims) % batchsize
    if undershoot != 0:
        batch = train_ims[-undershoot:].transpose(0,3,1,2)
        if nchannels == 1:
            batch = batch.repeat(3, axis=1)
        train_features[-undershoot:] = fn(
            batch.astype(np.float32))
    print 'done'

    print 'extracting test features...'
    for i in range(len(test_ims)//batchsize):
        print '{0}/{1}'.format(i, len(test_ims)//batchsize)
        start = (i * batchsize)
        end = ((i + 1) * batchsize)

        batch = test_ims[start:end].transpose(0,3,1,2)
        if nchannels == 1:
            batch = batch.repeat(3, axis=1)
        test_features[start:end] = fn(
            batch.astype(np.float32))

    undershoot = len(test_ims) % batchsize
    if undershoot != 0:
        batch = test_ims[-undershoot:].transpose(0,3,1,2)
        if nchannels == 1:
            batch = batch.repeat(3, axis=1)
        test_features[-undershoot:] = fn(
            batch.astype(np.float32))
    print 'done'


#n_val = 350
#label_counts = onehot(labels).sum(0)
#more_than_one_idx = np.where(label_counts > 1)[0]
#val_outputs = more_than_one_idx[numpy_rng.permutation(len(more_than_one_idx))[:350]]
#val_inputs = np.empty((n_val, ) + ims.shape[1:], dtype=ims.dtype)
#val_indices = []
#for i in range(n_val):
#    lbl_idx = np.where(labels == val_outputs[i])[0]
#    numpy_rng.shuffle(lbl_idx)
#    val_indices.append(lbl_idx[0])
#train_indices = list(set(np.arange(len(ims))).difference(val_indices))
#val_indices = list(val_indices)
#
#train_inputs = img_as_float(ims[train_indices]).astype(np.float32)
#train_inputs -= train_inputs.mean(1).mean(1)[:, None, None, :]
#val_inputs = img_as_float(ims[val_indices]).astype(np.float32)
#val_inputs -= val_inputs.mean(1).mean(1)[:, None, None, :]
#train_outputs = labels[train_indices].astype(np.int32)
#val_outputs = labels[val_indices].astype(np.int32)
#
#val_features = fn(
#    np.ascontiguousarray(val_inputs.transpose(0,3,1,2)[:,:,16:-16, 16:-16].repeat(3, axis=1))).reshape(-1,1024)
#val_labels = val_outputs
#
#
#def randcrop(im_stack, labels, patch_size, n_crops):
#    print 'shape of im_stack: {0}'.format(im_stack.shape, )
#    rval_im = np.zeros(
#        (im_stack.shape[0] * n_crops, patch_size[0], patch_size[1], im_stack.shape[3]),
#        dtype=im_stack.dtype)
#    rval_labels = np.repeat(labels, n_crops)
#    top = numpy_rng.randint(im_stack.shape[1]-patch_size[0], size=len(rval_im))
#    left = numpy_rng.randint(im_stack.shape[2]-patch_size[1], size=len(rval_im))
#    for r in range(len(rval_im)):
#        rval_im[r] = im_stack[r//n_crops,
#                           top[r]:top[r]+patch_size[0],
#                           left[r]:left[r]+patch_size[1]]
#    return rval_im, rval_labels
#
#train_features = []
#train_labels = []
#
#ntrain = train_inputs.shape[0]
#batchsize = 32
#for i in range(ntrain//batchsize):
#
#    print i
#    start = (i * batchsize) % len(train_inputs)
#    end = ((i + 1) * batchsize) % len(train_inputs)
#    print start, end
#    if end < start:
#        end = len(train_inputs)
#    print start, end
#    indices = np.arange(start, end)
#    if len(indices) < batchsize:
#        indices = np.concatenate((
#            indices,
#            np.arange(batchsize - len(indices))))
#
#    batch_inp, batch_outp = randcrop(
#        train_inputs[indices], labels[indices],
#        patch_size=(imsize, imsize), n_crops=4)
#    train_features.extend(fn(
#        batch_inp.transpose(0,3,1,2).repeat(3, axis=1),
#        .0).reshape(-1,1024))
#    train_labels.extend(batch_outp)
#missing = ntrain % batchsize
#if missing != 0:
#    indices = np.random.permutation(
#        len(train_inputs))[:missing]
#    batch_inp, batch_outp = randcrop(
#        train_inputs[indices], labels[indices],
#        patch_size=(imsize, imsize), n_crops=4)
#    train_features.extend(fn(batch_inp.transpose(0,3,1,2).repeat(3, axis=1),
#                             .0).reshape(
#        -1,1024))
#    train_labels.extend(batch_outp)
#
#print('dumping...')
#with tables.openFile('whale_features.h5', 'w') as h5file:
#    h5file.createArray(h5file.root, 'train_features', train_features)
#    h5file.createArray(h5file.root, 'train_labels', train_labels)
#    h5file.createArray(h5file.root, 'val_features', val_features)
#    h5file.createArray(h5file.root, 'val_labels', val_labels)







# vim: set ts=4 sw=4 sts=4 expandtab:
