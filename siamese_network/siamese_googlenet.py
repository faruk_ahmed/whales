#!/usr/bin/env python
#-*- coding: utf-8 -*-

# NOTE: use this fork: https://github.com/vmichals/sklearn-theano/

import os
import sys
sys.path.append('..')

import numpy as np
from sklearn_theano.feature_extraction import caffe, overfeat
import theano
from theano.sandbox.rng_mrg import MRG_RandomStreams
import theano.tensor as T

numpy_rng = np.random.RandomState(1)
theano_rng = MRG_RandomStreams(1)


print 'building theano graph of googlenet'
# build graph:
theanolayers, input_var, params = \
    caffe.googlenet.create_theano_expressions(
        return_params=True
    )
print 'done'

# vim: set ts=4 sw=4 sts=4 expandtab:
