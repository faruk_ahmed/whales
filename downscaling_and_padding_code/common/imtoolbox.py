#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
from PIL import Image
import numpy as np


def autocrop(im):
    im = np.asarray(im)
    if im.ndim == 2:
        im_bw = im
    elif im.ndim == 3:
        im_bw = im.max(axis=2)
    non_empty_columns = np.where(im_bw.max(axis=0)>0)[0]
    non_empty_rows = np.where(im_bw.max(axis=1)>0)[0]
    cropBox = (min(non_empty_rows), max(non_empty_rows), min(non_empty_columns), max(non_empty_columns))

    return im[cropBox[0]:cropBox[1]+1, cropBox[2]:cropBox[3]+1]


def imlist2gif(gif_filename, im_list, delay=20):
    os.system('convert -loop 0 -delay {0} {1} {2}'.format(
        delay, " ".join(im_list), gif_filename))


# vim: set ts=4 sw=4 sts=4 expandtab:
