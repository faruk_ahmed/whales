#!/usr/bin/env python
#-*- coding: utf-8 -*-

import numpy as np
from numpy.random import choice,rand,randint

from skimage import img_as_float, img_as_ubyte
from skimage.transform import warp, AffineTransform, rescale
from skimage.util import pad


def rand_transforms(
    im, n=1, scale_minmax=(1, 1), rot_max=0,
    translation_minmax_rel_to_imsize=(0, 0), randflip=(False, False), cval=0.,
    pad_max=0):
    """Randomly transforms the input image

    Args
    ----
        im: numpy ndarray (axis-order: rows, cols, ..., for videos transpose,
            such that first two axes are row,col and then retranspose the
            return value
        scale_minmax: tuple of min, max scale factors
        rot_max: max rotation angle (in degree)
        translation_minmax_rel_to_imsize: max translation relative to axis sizes
        randflip: sequence of boolean, indicating whether to randomly
            horizontally and or vertically flip the image
        n: number of random transformations to apply

    Returns
    -------
    Numpy ndarray of randomly transformed copies (1 axis more)
    """
    if isinstance(cval, str) and isinstance == 'mean':
        if np.issubdtype(im.dtype, np.float):
            cval = im.mean()
        else:
            cval = im.mean() / 255.
    if n == 0:
        return np.zeros((0, ) + im.shape, dtype=im.dtype)
    #reshaped = im.reshape(orig_shp[0], orig_shp[1], -1)
    reshaped = im.copy()
    rot_angles = np.radians(rand(n) * 2*rot_max - rot_max)
    scale_facs = rand(n) * (
        scale_minmax[1] - scale_minmax[0]) + scale_minmax[0]
    translation_vecs = rand(n, 2) * (
        translation_minmax_rel_to_imsize * np.array(im.shape[:2]))[None, :]
    if pad_max > 0:
        pad_seqs = np.hstack((
            randint(pad_max, size=(n, 2, 2)),
            np.zeros((n, im.ndim - 2, 2), dtype=int)))
    else:
        pad_seqs = np.zeros((n, 3, 2), dtype=int)

    # vertical/horizontal flip if corresponding element is -1 and if
    # randflip[0]/randflip[1] is True
    vert_flips = np.where(randflip[0], choice((-1,1), size=n),
                          np.ones(n, dtype=int))
    horz_flips = np.where(randflip[1], choice((-1,1), size=n),
                          np.ones(n, dtype=int))

    back_to_orig_dtype = lambda x: x.astype(im.dtype)
    if im.dtype == np.uint8:
        reshaped = img_as_float(reshaped)
        back_to_orig_dtype = lambda x: img_as_ubyte(x)
    elif not np.issubdtype(im.dtype, np.float):
        raise ValueError('expected either float or int image')


    origin_to_center = AffineTransform(
        translation=-np.array(reshaped.shape[:2]) / 2)

    origin_to_orig_coord = AffineTransform(
        translation=np.array(reshaped.shape[:2]) / 2)

    transfs = [
        origin_to_center + AffineTransform(
            translation=translation_vecs[i], rotation=rot_angles[i],
            scale=(scale_facs[i], scale_facs[i])) + origin_to_orig_coord
        for i in range(n)]

    return [back_to_orig_dtype(pad(
        warp(
            reshaped, transfs[i], mode='constant', cval=cval),
        pad_width=pad_seqs[i], mode='constant',
        constant_values=cval))[::vert_flips[i],::horz_flips[i]]
        for i in range(n)]


def rescale_and_pad(im, target_res, pad_val):
    back_to_orig_dtype = lambda x: x.astype(im.dtype)
    im2 = img_as_float(im)
    if im.dtype == np.uint8:
        im2 = img_as_float(im)
        back_to_orig_dtype = lambda x: img_as_ubyte(x).astype(im.dtype)

    elif not np.issubdtype(im.dtype, np.float):
        raise ValueError('expected either float or int image')

    scale_fac = float(target_res)/max(im2.shape[:2])
    im2 = rescale(
        im2, scale_fac, mode='constant', cval=pad_val)


    pad_h = (int(np.ceil((float(target_res) - im2.shape[0]) / 2)),
             int(np.floor((float(target_res) - im2.shape[0]) / 2)))
    pad_w = (int(np.ceil((float(target_res) - im2.shape[1]) / 2)),
             int(np.floor((float(target_res) - im2.shape[1]) / 2)))
    pad_ch = (0, 0)
    if im2.ndim == 2:
        pad_seqs = (pad_h, pad_w)
    elif im2.ndim == 3:
        pad_seqs = (pad_h, pad_w, pad_ch)
    else:
        raise ValueError('ndim not in (2,3)')
    return back_to_orig_dtype(pad(
        im2, pad_seqs, mode='constant',
        constant_values=pad_val))

def crop_rand_patches(im, patchsize, n=1):
    row = randint(im.shape[0] - patchsize, size=n)
    col = randint(im.shape[1] - patchsize, size=n)
    return np.vstack([im[None, r:r+patchsize, c:c+patchsize] for r,c in zip(
        row, col)])

def crop_rand_patches_imstack(ims, patchsize, n=1):
    im_idx = randint(len(ims))
    row = randint(ims.shape[0] - patchsize, size=n)
    col = randint(ims.shape[1] - patchsize, size=n)
    return np.vstack([ims[i:i+1, r:r+patchsize, c:c+patchsize] for i,r,c in zip(
        im_idx, row, col)])

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from video_dataset import VideoDataset
    from videotools import load_video_clip
    ntransf = 9

    clipset_dir = 'ClipSets'
    videodir = '.'
    fileext = ''
    labelsfile = 'emotiw2014_labels.h5'

    translation_minmax_rel_to_imsize = .1
    scale_minmax = (.9, 1.1)
    rot_max = np.radians(5.)

    dataset = VideoDataset(clipset_dir, videodir, labelsfile, fileext)

    trainfiles = dataset.gettrainfiles()
    trainlabels = dataset.gettrainlabels()

    vid = load_video_clip('{0}/{1}'.format(videodir, trainfiles[0]))

    # make random transformed copies (this is a video, so transpose, s.t. rows, col
    # axis are first two
    transformed_vids = rand_transforms(
        im=vid.transpose(1,2,0),
        n=ntransf,
        scale_minmax=scale_minmax,
        rot_max=rot_max,
        translation_minmax_rel_to_imsize=translation_minmax_rel_to_imsize,
        randflip=True)

    # move time axis back to front
    transformed_vids = [v.transpose(2,0,1) for v in transformed_vids]
    for i in range(9):
        plt.subplot(5, 2, i+1)
        plt.imshow(transformed_vids[i,0])
        plt.gray()
    plt.subplot(10)
    plt.imshow(transformed_vids[i,0])
    plt.imsave('/tmp/test.png')


# vim: set ts=4 sw=4 sts=4 expandtab:
