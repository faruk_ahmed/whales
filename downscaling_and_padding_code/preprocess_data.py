#!/usr/bin/env python
#-*- coding: utf-8 -*-

import csv
from PIL import Image
#from multiprocessing import Pool
import os

import numpy as np
import tables

from common.imagetools import rescale_and_pad

def preprocess_data(dataset_h5_path, training_set_dir, imsize):
    with open(os.path.join(training_set_dir, 'train.csv')) as train_file:
        # read csv and collect all file names and labels
        train_annotations = csv.reader(train_file)
        train_im_files = []
        train_labels = []
        # read linewise (skip header row)
        train_annotations.next()
        for row in train_annotations:
            im_file, lbl = row
            train_im_files.append(os.path.join(training_set_dir, 'imgs', row[0]))
            train_labels.append(row[1])

    # get the sorted list of unique labels
    classes = sorted(np.unique(train_labels))
    # and replace the labels by indexes into that list
    # so we have labels [0,...,m-1] and can use argmax in the classifier
    train_labels = np.array(map(classes.index, train_labels))

    def load_and_resize_im(imfile):
        print imfile
        im = np.array(Image.open(imfile))
        im_rescaled = rescale_and_pad(
            im, target_res=imsize, pad_val=0)
        return im_rescaled
    train_ims = []
    for i, imfile in enumerate(train_im_files):
        print '{0}/{1}'.format(i+1, len(train_im_files))
        train_ims.append(load_and_resize_im(imfile))

    train_ims = np.array(train_ims)

    # dump into hdf5 file
    with tables.openFile(dataset_h5_path, 'w') as h5file:
        h5file.createArray(h5file.root, 'classes', np.array(classes))
        h5file.createArray(h5file.root, 'train_labels', train_labels)
        h5file.createArray(h5file.root, 'train_ims', train_ims)

# vim: set ts=4 sw=4 sts=4 expandtab:
