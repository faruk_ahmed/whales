#!/usr/bin/env python
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from skimage import img_as_float
import tables

from common.misc import onehot


imsize = 256
dataset_h5_path = '/storeSSD/data/whales/whales_{0}x{1}.h5'.format(
    imsize, imsize)
training_set_dir = '/storeSSD/data/whales/data/'

print 'loading data...'
def load_data():
    with tables.openFile(dataset_h5_path) as h5file:
        train_ims = h5file.root.train_ims.read()
        train_labels = h5file.root.train_labels.read()
        classes = h5file.root.classes.read()
    return train_ims, train_labels, classes

try:
    ims, labels, classes = load_data()
except (IOError, tables.exceptions.NoSuchNodeError):
    from preprocess_data import preprocess_data
    preprocess_data(dataset_h5_path, training_set_dir, imsize)
    ims, labels, classes = load_data()


numpy_rng = np.random.RandomState(1)

# we have 4544 images, split off 350 for validation (make sure to leave one
# sample of each class in the train set, some classes only have one!)
n_val = 350
label_counts = onehot(labels).sum(0)
more_than_one_idx = np.where(label_counts > 1)[0]
val_outputs = more_than_one_idx[numpy_rng.permutation(len(more_than_one_idx))[:350]]
val_inputs = np.empty((n_val, ) + ims.shape[1:], dtype=ims.dtype)

val_indices = []
for i in range(n_val):
    lbl_idx = np.where(labels == val_outputs[i])[0]
    numpy_rng.shuffle(lbl_idx)
    val_indices.append(lbl_idx[0])
train_indices = list(set(np.arange(len(ims))).difference(val_indices))
val_indices = list(val_indices)

train_inputs = img_as_float(ims[train_indices]).astype(np.float32)
plt.imsave('tmp/test.png', ims[0])
plt.imsave('tmp/test2.png', train_inputs[0])
train_inputs -= train_inputs.mean(1).mean(1)[:, None, None, :]
val_inputs = img_as_float(ims[val_indices]).astype(np.float32)
val_inputs -= val_inputs.mean(1).mean(1)[:, None, None, :]
train_outputs = labels[train_indices].astype(np.int32)
val_outputs = labels[val_indices].astype(np.int32)



# vim: set ts=4 sw=4 sts=4 expandtab:
